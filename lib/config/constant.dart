

class Constant {
  static final apiUrl = 'http://localhost:8080/api/';

  static String formatDateHour = 'dd/MM/yyyy\nHH:mm';

  static String formatDate = 'dd/MM/yyyy';

  static String formatDateHour1 = 'HH:mm-dd/MM/yyyy';

  static String formatMonth = 'MM/yyyy';

}
