enum Environment { DEV, STAGING, PROD }

class EnvConfig {
  static Map<String, dynamic> _config;

  static void setEnvironment(Environment env) {
    switch (env) {
      case Environment.DEV:
        _config = _Config.debugConstants;
        break;
      case Environment.PROD:
        _config = _Config.prodConstants;
        break;
      default:
        _config = _Config.debugConstants;
    }
  }

  static get apiUrl {
    return _config[_Config.API_URL];
  }

}

class _Config {
  static const API_URL = 'API_URL';

  static Map<String, dynamic> debugConstants = {
    API_URL: 'http://localhost:8080/api/'
  };

  static Map<String, dynamic> prodConstants = {
    API_URL: 'http://192.168.165.101:8080/api/'
  };
}
