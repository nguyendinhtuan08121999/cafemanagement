import 'package:cafe_management/styles/component.dart';
import 'package:flutter/material.dart';

enum AppTheme {
  Light,
  Dark,
}

String enumName(AppTheme anyEnum) {
  return anyEnum.toString().split('.')[1];
}

final appThemeData = {
  AppTheme.Light: ThemeData(
    disabledColor: Color(0xFF8E9AB5),
    brightness: Brightness.light,
    primaryColor: Color(0xffFFFFFF),
    primarySwatch: MaterialColors.colorLight,
    scaffoldBackgroundColor: Color(0xffECEFF9),
    accentColor: Color(0xffD9702D),
    errorColor: Color(0xFFCE1313),
    //fontFamily: 'Lato',
    backgroundColor: Colors.white,
    buttonColor: Color(0xFF3967D5),
    dialogBackgroundColor: Colors.white,
    shadowColor: Color(0xFFDDDEDF),
    hintColor: Color(0xFFC7C7C7),
    buttonTheme: ButtonThemeData(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      buttonColor: Color(0xFF3967D5),
      disabledColor: Color(0xFF8E9AB5),
      splashColor: Color(0xFF2C56B9),
      textTheme: ButtonTextTheme.primary,
    ),
    appBarTheme: AppBarTheme(
      elevation: 2,
      color: Color(0xFFFFFFFF),
      shadowColor: Color(0xFFDDDEDF),
      brightness: Brightness.light,
      iconTheme: IconThemeData(
        color: Color(0xFF3967D5),
      ),
    ),
    dialogTheme: DialogTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
    cardTheme: CardTheme(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      elevation: 2,
      shadowColor: Color(0xFFDDDEDF),
    ),
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Color(0xff122641 ), fontWeight: FontWeight.w300, fontSize: 12,),
      bodyText2: TextStyle(color: Color(0xFF838383), fontWeight: FontWeight.w300, fontSize: 12,),
      button: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.white,),
    ),
  ),
  AppTheme.Dark: ThemeData(
    scaffoldBackgroundColor: Color(0xff022640),
  ),
};
