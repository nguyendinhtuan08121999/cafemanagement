import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/plugin/locator.dart';
import 'package:cafe_management/plugin/navigator.dart';
import 'package:cafe_management/providers/common/theme-provider.dart';
import 'package:cafe_management/providers/common/token-provider.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/router/router.dart';
import 'package:cafe_management/screens/splash/body.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';
import 'config/env-config.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeManager>(
      builder: (context, manager, child) {
        return new MaterialApp(
          title: 'CoffeeManagement',
          routes: RoutesConstant.routes,
          debugShowCheckedModeBanner: false,
          navigatorKey: locator<NavigationService>().navigatorKey,
          theme: manager.themeData,
          home: new BodySplash(),
        );
      },
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  EnvConfig.setEnvironment(Environment.DEV);
  runApp(
    Phoenix(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => HoaDonProvider()),
          ChangeNotifierProvider(create: (context) => MonAnProvider()),
          ChangeNotifierProvider(create: (context) => UserProvider()),
          ChangeNotifierProvider(create: (context) => TableProvider()),
          ChangeNotifierProvider(create: (context) => ThemeManager()),
          ChangeNotifierProvider(create: (context) => TokenProvider()),
        ],
        child: MyApp(),
      ),
    ),
  );
}
