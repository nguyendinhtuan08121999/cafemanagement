class LanguageConfig {
  String code;

  String name;

  String flag;

  bool status;

  int order;

  LanguageConfig({ this.code, this.name, this.flag, this.order, this.status });

  factory LanguageConfig.fromJson(Map<String, dynamic> json) {
    return LanguageConfig(
      code: json['code'],
      name: json['name'],
      flag: json['flag'],
      order: json['order'],
      status: json['status']
    );
  }
}
