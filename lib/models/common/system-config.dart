class SystemConfig {
  String author;

  bool isAllowSwap;

  bool isAllowTransfer;

  bool isAllowWithdraw;

  int systemStatus;

  String siteName;

  var ignoreConfirm;

  String versionLatest;

  String versionRequireUpdate;

  SystemConfig({
    this.author,
    this.isAllowSwap,
    this.isAllowTransfer,
    this.isAllowWithdraw,
    this.systemStatus,
    this.siteName,
    this.ignoreConfirm,
    this.versionLatest,
    this.versionRequireUpdate,
  });

  factory SystemConfig.fromJson(Map<String, dynamic> json) {
    return SystemConfig(
        author: json['author'],
        isAllowSwap: json['isAllowSwap'],
        isAllowTransfer: json['isAllowTransfer'],
        isAllowWithdraw: json['isAllowWithdraw'],
        systemStatus: json['systemStatus'],
        siteName: json['siteName'],
        ignoreConfirm: json['ignoreConfirm'],
        versionLatest: json['versionLatest'],
        versionRequireUpdate: json['versionRequireUpdate']);
  }
}
