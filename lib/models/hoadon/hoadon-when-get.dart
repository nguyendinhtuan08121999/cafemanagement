import 'dart:convert';

import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/models/table/table.dart';

List<HoaDonGet> hoaDonGetFromJson(String str) => List<HoaDonGet>.from(json.decode(str).map((x) => HoaDonGet.fromJson(x)));

class HoaDonGet {
  HoaDonGet({
    this.maHoaDon,
    this.ban,
    this.tenNhanVien,
    this.thoiGian,
    this.tongTien,
    this.daThanhToan,
    this.listChiTietHoaDon,
  });

  int maHoaDon;
  TableCoffee ban;
  String tenNhanVien;
  DateTime thoiGian;
  double tongTien;
  bool daThanhToan;
  List<ListChiTietHoaDon> listChiTietHoaDon;

  factory HoaDonGet.fromJson(Map<String, dynamic> json) => HoaDonGet(
    maHoaDon: json["maHoaDon"],
    ban: TableCoffee.fromJson(json["ban"]),
    tenNhanVien: json["tenNhanVien"] == null ? null : json["tenNhanVien"],
    thoiGian: DateTime.parse(json["thoiGian"]),
    tongTien: json["tongTien"] == null ? null : json["tongTien"],
    daThanhToan: json["daThanhToan"],
    listChiTietHoaDon: List<ListChiTietHoaDon>.from(json["listChiTietHoaDon"].map((x) => ListChiTietHoaDon.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "maHoaDon": maHoaDon,
    "ban": ban.toJson(),
    "tenNhanVien": tenNhanVien == null ? null : tenNhanVien,
    "thoiGian": thoiGian.toIso8601String(),
    "tongTien": tongTien == null ? null : tongTien,
    "daThanhToan": daThanhToan,
    "listChiTietHoaDon": List<dynamic>.from(listChiTietHoaDon.map((x) => x.toJson())),
  };
}

class ListChiTietHoaDon {
  ListChiTietHoaDon({
    this.maCTHoaDon,
    this.monAn,
    this.soLuong,
    this.donGia,
    this.ghiChu,
  });

  int maCTHoaDon;
  MonAn monAn;
  int soLuong;
  double donGia;
  String ghiChu;

  factory ListChiTietHoaDon.fromJson(Map<String, dynamic> json) => ListChiTietHoaDon(
    maCTHoaDon: json["maCTHoaDon"],
    monAn: MonAn.fromJson(json["monAn"]),
    soLuong: json["soLuong"],
    donGia: json["donGia"],
    ghiChu: json["ghiChu"],
  );

  Map<String, dynamic> toJson() => {
    "maCTHoaDon": maCTHoaDon,
    "monAn": monAn.toJson(),
    "soLuong": soLuong,
    "donGia": donGia,
    "ghiChu": ghiChu,
  };
}