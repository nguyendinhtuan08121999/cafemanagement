// To parse this JSON data, do
//
//     final hoaDon = hoaDonFromJson(jsonString);

import 'dart:convert';

HoaDon hoaDonFromJson(String str) => HoaDon.fromJson(json.decode(str));

String hoaDonToJson(HoaDon data) => json.encode(data.toJson());

class HoaDon {
  HoaDon({
    this.ban,
    this.thoiGian,
    this.tongTien,
    this.daThanhToan,
    this.tenNhanVien,
    this.listChiTietHoaDon,
  });

  Ban ban;
  DateTime thoiGian;
  dynamic tongTien;
  bool daThanhToan;
  String tenNhanVien;
  List<ListChiTietHoaDon> listChiTietHoaDon;

  factory HoaDon.fromJson(Map<String, dynamic> json) => HoaDon(
    ban: Ban.fromJson(json["ban"]),
    thoiGian: DateTime.parse(json["thoiGian"]),
    tongTien: json["tongTien"],
    daThanhToan: json["daThanhToan"],
    tenNhanVien: json["tenNhanVien"],
    listChiTietHoaDon: List<ListChiTietHoaDon>.from(json["listChiTietHoaDon"].map((x) => ListChiTietHoaDon.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "ban": ban.toJson(),
    "thoiGian": thoiGian.toIso8601String(),
    "tongTien": tongTien,
    "daThanhToan": daThanhToan,
    "tenNhanVien": tenNhanVien,
    "listChiTietHoaDon": List<dynamic>.from(listChiTietHoaDon.map((x) => x.toJson())),
  };
}

class Ban {
  Ban({
    this.maBan,
  });

  int maBan;

  factory Ban.fromJson(Map<String, dynamic> json) => Ban(
    maBan: json["maBan"],
  );

  Map<String, dynamic> toJson() => {
    "maBan": maBan,
  };
}

class ListChiTietHoaDon {
  ListChiTietHoaDon({
    this.monAn,
    this.soLuong,
    this.donGia,
    this.ghiChu,
  });

  Monan monAn;
  int soLuong;
  double donGia;
  String ghiChu;

  factory ListChiTietHoaDon.fromJson(Map<String, dynamic> json) => ListChiTietHoaDon(
    monAn: Monan.fromJson(json["monAn"]),
    soLuong: json["soLuong"],
    donGia: json["donGia"],
    ghiChu: json["ghiChu"],
  );

  Map<String, dynamic> toJson() => {
    "monAn": monAn.toJson(),
    "soLuong": soLuong,
    "donGia": donGia,
    "ghiChu": ghiChu,
  };
}

class Monan {
  Monan({
    this.maMonAn,
  });

  int maMonAn;

  factory Monan.fromJson(Map<String, dynamic> json) => Monan(
    maMonAn: json["maMonAn"],
  );

  Map<String, dynamic> toJson() => {
    "maMonAn": maMonAn,
  };
}
