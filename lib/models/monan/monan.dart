class MonAn {
  num donGia;
  String hinhAnh;
  int maMonAn;
  String tenMonAn;

  MonAn({this.donGia, this.hinhAnh, this.maMonAn, this.tenMonAn});

  factory MonAn.fromJson(Map<String, dynamic> json) {
    return MonAn(
        maMonAn: json['maMonAn'],
        tenMonAn: json['tenMonAn'],
        donGia: json['donGia'],
        hinhAnh: json['hinhAnh']
    );
  }
  Map<String, dynamic> toJson() {
    return <String, dynamic> {
      'maMonAn': maMonAn,
      'tenMonAn': tenMonAn,
      'donGia': donGia,
      'hinhAnh': hinhAnh
    };
  }
}