
class TableCoffee {
  int maBan;
  String moTa; //là string nhưng phải điền số vì api nó bắt thế :v
  String tinhTrang; //defaul is blank
  String ghiChu;

  TableCoffee({this.maBan, this.moTa, this.tinhTrang, this.ghiChu});

  factory TableCoffee.fromJson(Map<String, dynamic> json) {
    return TableCoffee(
      maBan: json['maBan'],
      moTa: json['moTa'],
      tinhTrang: json['tinhTrang'],
      ghiChu: json['ghiChu']
    );
  }
  Map<String, dynamic> toJson() {
    return <String, dynamic> {
      'maBan': maBan,
      'moTa': moTa,
      'tinhTrang': tinhTrang,
      'ghiChu': ghiChu
    };
  }
}