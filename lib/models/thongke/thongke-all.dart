// To parse this JSON data, do
//
//     final thongKeAll = thongKeAllFromJson(jsonString);

import 'dart:convert';

ThongKeAll thongKeAllFromJson(String str) => ThongKeAll.fromJson(json.decode(str));

String thongKeAllToJson(ThongKeAll data) => json.encode(data.toJson());

class ThongKeAll {
  ThongKeAll({
    this.doanhSoCuaNhanVien,
    this.from,
    this.to,
    this.soLuongMon,
  });

  List<DoanhSoCuaNhanVien> doanhSoCuaNhanVien;
  String from;
  String to;
  List<SoLuongMon> soLuongMon;

  factory ThongKeAll.fromJson(Map<String, dynamic> json) => ThongKeAll(
    doanhSoCuaNhanVien: List<DoanhSoCuaNhanVien>.from(json["doanhSoCuaNhanVien"].map((x) => DoanhSoCuaNhanVien.fromJson(x))),
    from: json["from"],
    to: json["to"],
    soLuongMon: List<SoLuongMon>.from(json["soLuongMon"].map((x) => SoLuongMon.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "doanhSoCuaNhanVien": List<dynamic>.from(doanhSoCuaNhanVien.map((x) => x.toJson())),
    "from": from,
    "to": to,
    "soLuongMon": List<dynamic>.from(soLuongMon.map((x) => x.toJson())),
  };
}

class DoanhSoCuaNhanVien {
  DoanhSoCuaNhanVien({
    this.tenNhanVien,
    this.soHoaDon,
    this.doanhThu,
  });

  String tenNhanVien;
  int soHoaDon;
  num doanhThu;

  factory DoanhSoCuaNhanVien.fromJson(Map<String, dynamic> json) => DoanhSoCuaNhanVien(
    tenNhanVien: json["tenNhanVien"],
    soHoaDon: json["soHoaDon"],
    doanhThu: json["doanhThu"],
  );

  Map<String, dynamic> toJson() => {
    "tenNhanVien": tenNhanVien,
    "soHoaDon": soHoaDon,
    "doanhThu": doanhThu,
  };
}

class SoLuongMon {
  SoLuongMon({
    this.maMonAn,
    this.tenMonAn,
    this.soLuong,
  });

  int maMonAn;
  String tenMonAn;
  int soLuong;

  factory SoLuongMon.fromJson(Map<String, dynamic> json) => SoLuongMon(
    maMonAn: json["maMonAn"],
    tenMonAn: json["tenMonAn"],
    soLuong: json["soLuong"],
  );

  Map<String, dynamic> toJson() => {
    "maMonAn": maMonAn,
    "tenMonAn": tenMonAn,
    "soLuong": soLuong,
  };
}
