// To parse this JSON data, do
//
//     final thongKe = thongKeFromJson(jsonString);

import 'dart:convert';

ThongKe thongKeFromJson(String str) => ThongKe.fromJson(json.decode(str));

String thongKeToJson(ThongKe data) => json.encode(data.toJson());

class ThongKe {
  ThongKe({
    this.id,
    this.thang,
    this.soluonghoadon,
    this.soluongcoc,
    this.doanhthu,
  });

  int id;
  int thang;
  int soluonghoadon;
  int soluongcoc;
  num doanhthu;

  factory ThongKe.fromJson(Map<String, dynamic> json) => ThongKe(
    id: json["id"],
    thang: json["thang"],
    soluonghoadon: json["soluonghoadon"],
    soluongcoc: json["soluongcoc"],
    doanhthu: json["doanhthu"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "thang": thang,
    "soluonghoadon": soluonghoadon,
    "soluongcoc": soluongcoc,
    "doanhthu": doanhthu,
  };
}
