import 'dart:convert';
import 'dart:io';

import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/providers/common/token-provider.dart';
import 'package:cafe_management/response/response-data.dart';
import 'package:cafe_management/router/routing-name.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'locator.dart';
import 'navigator.dart';


final NavigationService _navigationService = locator<NavigationService>();

Dio http = Dio(
  BaseOptions(
    connectTimeout: 10000,
    responseType: ResponseType.json,
    contentType: ContentType.json.toString(),
  ),
)..interceptors.addAll([buildInterceptorsWrapper()]);

buildInterceptorsWrapper() {
  return InterceptorsWrapper(
    onRequest: (RequestOptions requestOptions) async {
      http.interceptors.requestLock.lock();
      TokenObj tokenObj = await SharedPrefsService.getTokenObj();
      if (tokenObj != null && tokenObj.accessToken != null && tokenObj.accessToken != ''
        && requestOptions.uri.toString().indexOf('login') == -1) {
        requestOptions.headers['Authorization'] = "Bearer " + tokenObj.accessToken;
        http.interceptors.requestLock.unlock();
        return requestOptions;
      }
      http.interceptors.requestLock.unlock();
      return requestOptions;
    },
    onResponse: (Response response) async {
      // Do something with response data
      // if (response.data['status'] != null) {
      //   response.data['status'] = response.data['status'] == Constant.successText;
      // }
      // if (response.request.path.indexOf('login') != -1) {
      //   response.data['data'] = response.data['tokenObject'];
      // }
      // try {
      //   if (response.data['data']['totalPages'] != null) {
      //     Map<String, dynamic> map = {};
      //     if (response.data['pagination'] == null) response.data['pagination'] = map;
      //     response.data['pagination']['page'] = response.data['data']['number'] + 1;
      //     response.data['pagination']['size'] = response.data['data']['size'];
      //     response.data['pagination']['total'] = response.data['data']['totalPages'];
      //     response.data['pagination']['totalRecords'] = response.data['data']['totalElements'];
      //   }
      // } catch (e) {
      //   response.data['pagination'] = {};
      // }
      // ResponseData responseData = ResponseData.fromJson(response.data);
      // response.data = responseData;
      return response;// continue
    },
    onError: (DioError error) async {
      try {
        switch (error.type) {
          case DioErrorType.CANCEL:
            print('Request Cancelled');
            break;
          case DioErrorType.CONNECT_TIMEOUT:
            print('DioErrorType.CONNECT_TIMEOUT');
            break;
          case DioErrorType.DEFAULT:
            print('DioErrorType.DEFAULT ${error.response}');
            break;
          case DioErrorType.RECEIVE_TIMEOUT:
            print('DioErrorType.RECEIVE_TIMEOUT');
            break;
          case DioErrorType.RESPONSE:
            print('Request with response error ${error.response}');
            break;
          case DioErrorType.SEND_TIMEOUT:
            break;
        }
      } catch (e) {
        return error;
      }
      Map map = jsonDecode(error.response.toString()) ?? {};
      error?.response?.data = ResponseData(false, map['message'] ?? 'Error', null, null);
      return error.response;
    },
  );
}
