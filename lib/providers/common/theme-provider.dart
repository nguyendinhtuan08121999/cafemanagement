import 'package:cafe_management/config/theme.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:flutter/material.dart';

class ThemeManager with ChangeNotifier {

  ThemeManager() {
    _loadTheme();
  }

  ThemeData _themeData;
  
  /// Use this method on UI to get selected theme.
  ThemeData get themeData {
    return appThemeData[AppTheme.Light];
  }

  /// Sets theme and notifies listeners about change. 
  setTheme(AppTheme theme) async {
    _themeData = appThemeData[theme];
    SharedPrefsService.setThemeMode(theme);
    notifyListeners();
  }

  void _loadTheme() async {
    _themeData = await SharedPrefsService.getThemeMode();
    notifyListeners();
  }

}
