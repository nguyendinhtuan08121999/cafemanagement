import 'dart:convert';

import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:flutter/foundation.dart';

class TokenProvider extends ChangeNotifier {
  TokenObj get tokenObj => _tokenObj;

  TokenObj _tokenObj;

  bool get isLogged => _isLogged;

  bool _isLogged = false;

  Future<bool> getTokenObj() async {
    _tokenObj = await SharedPrefsService.getTokenObj();
    if (_tokenObj != null && _tokenObj.accessToken != null && _tokenObj.accessToken != '') {
      print('get token ${tokenObj.accessToken}');
      _isLogged = true;
    } else {
      _isLogged = false;
    }
    notifyListeners();
    return _isLogged;
  }

  Future<void> setTokenObj(String str) async {
    await SharedPrefsService.setTokenObj(str);
    //Map map = jsonDecode(str);
    _tokenObj = TokenObj.fromJson(str);
    _isLogged = true;
    notifyListeners();
  }

  void removeToken() {
    SharedPrefsService.removeToken();
    _tokenObj = new TokenObj();
    _isLogged = false;
    notifyListeners();
  }

  void setExpiredState() {
    _isLogged = false;
    notifyListeners();
  }
}
