import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:flutter/cupertino.dart';

class HoaDonProvider extends ChangeNotifier{
  bool load = true;
  List<HoaDonGet> hoaDonGet = new List<HoaDonGet>();
  List<HoaDonGet> hoaDonPaid = new List<HoaDonGet>();
  List<HoaDonGet> hoaDonUnPaid = new List<HoaDonGet>();

  void clearHoaDon() {

  }

  HoaDonGet getByTable({int maBan}) {
    for (int i = 0; i < hoaDonUnPaid.length; i++) {
      if (hoaDonUnPaid[i].ban.maBan == maBan)
        return hoaDonUnPaid[i];
    }
  }

  void getHoaDon() async{
    List <dynamic> li = await MonAnService.getHoaDon();
    hoaDonGet.clear();
    hoaDonPaid.clear();
    hoaDonUnPaid.clear();
    for (int i = 0; i < li.length; i++) {
      HoaDonGet hd = HoaDonGet.fromJson(li[i]);
      hoaDonGet.add(hd);
      if (hd.daThanhToan) {
        hoaDonPaid.add(hd);
      } else
        hoaDonUnPaid.add(hd);
    }
    load = false;
    notifyListeners();
  }
}