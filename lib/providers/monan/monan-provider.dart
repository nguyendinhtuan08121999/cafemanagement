import 'package:cafe_management/models/hoadon/hoadon.dart';
import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:flutter/cupertino.dart';

class MonAnProvider extends ChangeNotifier {
  bool isFavou = false;
  bool load = true;
  List<MonAn>  liMonAn = new List<MonAn>();
  List<MonAn>  liMonAn1 = new List<MonAn>();
  List<MonAn>  liMonAn2 = new List<MonAn>();
  List<MonAn>  liMonAn3 = new List<MonAn>();

  final _productsInCart = <int, int>{};

  Map<int, int> get productsInCart {
    return Map.from(_productsInCart);
  }

  MonAn getProductById(int id) {
    return liMonAn.firstWhere((p) => p.maMonAn == id);
  }

  int get totalCartQuantity {
    return _productsInCart.values.fold(0, (accumulator, value) {
      return accumulator + value;
    });
  }

  double get subtotalCost {
    return _productsInCart.keys.map((id) {
      // Extended price for product line
      return getProductById(id).donGia * _productsInCart[id];
    }).fold(0, (accumulator, extendedPrice) {
      return accumulator + extendedPrice;
    });
  }

  void addProductToCart(int productId, int sluong) {
    if (!_productsInCart.containsKey(productId)) {
      _productsInCart[productId] = sluong;
    } else {
      _productsInCart[productId]+= sluong;
    }
    notifyListeners();
  }

  // Removes an item from the cart.
  void removeItemFromCart(int productId) {
    if (_productsInCart.containsKey(productId)) {
      if (_productsInCart[productId] == 1) {
        _productsInCart.remove(productId);
      } else {
        _productsInCart[productId]--;
      }
    }
    notifyListeners();
  }

  void getMonAn() async{
    liMonAn = await MonAnService.getAllMonAn();
    liMonAn1 = await MonAnService.getAllMonAn1();
    liMonAn2 = await MonAnService.getAllMonAn2();
    liMonAn3 = await MonAnService.getAllMonAn3();
    load = false;
    notifyListeners();
  }

  void clearCart() {
    _productsInCart.clear();
    notifyListeners();
  }

  List<dynamic> listCTHD() {
    List<Map<String, dynamic>> list = new List<Map<String, dynamic>>();
    for (int i=0; i <_productsInCart.length; i++) {
      Monan mon = new Monan();
      mon.maMonAn = _productsInCart.keys.toList()[i];
      ListChiTietHoaDon CTHoaDon = new ListChiTietHoaDon();
      CTHoaDon.monAn = mon;
      CTHoaDon.ghiChu = 'No comment';
      CTHoaDon.soLuong = _productsInCart.values.toList()[i];
      CTHoaDon.donGia = _productsInCart.values.toList()[i] * getProductById(_productsInCart.keys.toList()[i]).donGia;
      print('CTHD:' + CTHoaDon.donGia.toString());
      //Map <String, dynamic> mapCTHD= CTHoaDon.toJson();
      list.add(CTHoaDon.toJson());
    }
    return list;
  }

}