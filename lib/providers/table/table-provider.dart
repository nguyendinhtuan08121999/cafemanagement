import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/services/table-service.dart';
import 'package:flutter/cupertino.dart';

class TableProvider extends ChangeNotifier {
  bool load = true;
  List<TableCoffee>  litable = [];
  TableCoffee table;

  void getTable() async{
    litable = await TableService.getAllTable();
    load = false;
    notifyListeners();
  }
}