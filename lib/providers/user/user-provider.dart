import 'package:cafe_management/models/user/user.dart';
import 'package:cafe_management/services/user-service.dart';
import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier {
  User user;

  void getUser(String str) async {
    user = await UserService.getUser(str);
    notifyListeners();
  }
  void removeUser(){
    user = null;
    notifyListeners();
  }
}
