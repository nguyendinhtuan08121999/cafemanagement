import 'package:cafe_management/router/routing-name.dart';
import 'package:cafe_management/screens/layout/main.dart';
import 'package:cafe_management/screens/splash/body.dart';
import 'package:cafe_management/sign-in/sign_in_screen.dart';
import 'package:flutter/material.dart';

abstract class RoutesConstant {
  static final routes = <String, WidgetBuilder> {
    RoutingNameConstant.homeRoute: (BuildContext context) => new DashboardHomePage(),
    RoutingNameConstant.splashScreen: (BuildContext context) => new BodySplash(),
    RoutingNameConstant.loginScreen: (BuildContext context) => new SignInScreen(),
  };
}
