abstract class RoutingNameConstant {
  static const String homeRoute = '/dashboard';
  static const String splashScreen = '/splash';
  static const String loginScreen = '/login';
  static const String homeCustomer = '/customer';
}
