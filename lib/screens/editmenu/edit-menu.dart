import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/screens/editmenu/widgets/add-coffee-item.dart';
import 'package:cafe_management/screens/editmenu/widgets/edit-coffee-item.dart';
import 'package:cafe_management/screens/editmenu/widgets/edit-product-row-item.dart';
import 'package:cafe_management/screens/editmenu/widgets/loai-mon-edit.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/widgets/item-product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditMenuScreen extends StatefulWidget {
  @override
  _TimeSheetScreenState createState() => _TimeSheetScreenState();
}

class _TimeSheetScreenState extends State<EditMenuScreen> {
  @override
  void initState() {
    Provider.of<MonAnProvider>(context, listen: false).getMonAn();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
     return Scaffold(
       floatingActionButton: FloatingActionButton(
         onPressed: () {
           Navigator.push(context, MaterialPageRoute(builder: (context) => LoaiMonEdit()));
         },
         child: Icon(Icons.add),
         backgroundColor: Color(0xff4893AB),
       ),
      appBar: AppbarBuilder.primaryAppbar('Edit menu'),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, left: 35, right: 30),
        child: Consumer<MonAnProvider>(
          builder: (context, model, child) {
            return GridView.builder(
              itemCount: model.liMonAn.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 20,
                crossAxisSpacing: 50,
                childAspectRatio: 0.8,
              ),
              itemBuilder: (context, index) => ItemProduct(
                monAn: model.liMonAn[index],
                press: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditCoffeeItem(monAn: model.liMonAn[index],)));
                },
              )
            );
          },
        ),
      ),
    );
  }
  Widget _buildCard({MonAn monAn}) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => EditCoffeeItem(monAn: monAn,)));
      },
      child: Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: Container(
          padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3.0,
                blurRadius: 5.0
              )
            ],
            color: Colors.white,
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(Icons.favorite_border_outlined, color: Color(0xFFEF7532))
                  ]
                )
              ),
              Container(
                height: 75.0,
                width: 75.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("images/coffeesplash3.png"),
                    fit: BoxFit.contain,
                  )
                )
              ),
              SizedBox(height: 7.0),
              Text(numberToMoneyString1(monAn.donGia),
                style: TextStyle(
                  color: Color(0xFFCC8053),
                  fontFamily: 'Varela',
                  fontSize: 14.0)),
              Text(monAn.tenMonAn,
                style: TextStyle(
                  color: Color(0xFF575E67),
                  fontFamily: 'Varela',
                  fontSize: 14.0)),
              Padding(
                padding: EdgeInsets.only(left:8, top: 8, right: 8, bottom: 4),
                child: Container(color: Color(0xFFEBEBEB), height: 1.0)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Edit',
                    style: TextStyle(
                      fontFamily: 'Varela',
                      color: Color(0xFFD17E50),
                      fontSize: 14.0
                    )
                  )
                ],
              )
            ]
          )
        )
      ),
    );
  }
}
