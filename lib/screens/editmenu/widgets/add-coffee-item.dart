
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/textfield-outline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddCoffeeItem extends StatefulWidget {
  final int maLoaiMon;

  AddCoffeeItem({this.maLoaiMon});
  @override
  _DetailCoffeeItemState createState() => _DetailCoffeeItemState();
}

class _DetailCoffeeItemState extends State<AddCoffeeItem> {
  TextEditingController idController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Insert Item', true, context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Center(
                //   child: GestureDetector(
                //     onTap: () {
                //       _showPicker(context);
                //     },
                //     child: CircleAvatar(
                //       radius: 55,
                //       backgroundColor: Colors.brown,
                //       child: _image != null
                //       ? ClipRRect(
                //         borderRadius: BorderRadius.circular(50),
                //         child: Image.file(
                //           _image,
                //           width: 100,
                //           height: 100,
                //           fit: BoxFit.fitHeight,
                //         ),
                //       )
                //       : Container(
                //         decoration: BoxDecoration(
                //           color: Colors.grey[200],
                //           borderRadius: BorderRadius.circular(50),
                //         ),
                //         width: 100,
                //         height: 100,
                //         child: Icon(
                //           Icons.camera_alt,
                //           color: Colors.grey[800],
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
                // Text('ID:', style: CommonTextStyle.size14Medium,),
                // SizedBox(height: 8,),
                // TextFieldOutLine(
                //   controller: idController,
                // ),
                // SizedBox(height: 15,),
                Text('Name:', style: CommonTextStyle.size14Medium,),
                SizedBox(height: 8,),
                TextFieldOutLine(
                  controller: nameController,
                ),
                SizedBox(height: 15,),
                Text('Price:', style: CommonTextStyle.size14Medium,),
                SizedBox(height: 8,),
                TextFieldOutLine(
                  isEnable: true,
                  controller: priceController,
                ),
                SizedBox(height: 20,),
                Align(
                  alignment: Alignment.centerRight,
                  child: RaisedButton(
                    color: Colors.brown,
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    onPressed: () async {
                      Map<String, dynamic> map = {
                        'tenMonAn' : nameController.text,
                        'donGia': priceController.text,
                        'loaiMon': {
                          "maLoaiMon": widget.maLoaiMon,
                        }
                      };
                      MonAnService.addMonAn(map).then((value) {
                        if (value.statusCode == 200) {
                          SnackbarBuilder.showSnackbar(content: 'Success', color: Colors.blue);
                          Provider.of<MonAnProvider>(context, listen: false).getMonAn();
                          Navigator.pop(context);
                          Navigator.pop(context);
                        } else {
                          SnackbarBuilder.showSnackbar(content: 'Error', color: Colors.red);
                        }
                      });
                    },
                    child: Text('Submit',),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}
