import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:cafe_management/widgets/textfield-outline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditCoffeeItem extends StatefulWidget {
  final MonAn monAn;
  EditCoffeeItem({this.monAn});

  @override
  _DetailCoffeeItemState createState() => _DetailCoffeeItemState();
}

class _DetailCoffeeItemState extends State<EditCoffeeItem> {
  TextEditingController idController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();

  @override
  void initState() {
    idController.text = widget.monAn.maMonAn.toString();
    nameController.text = widget.monAn.tenMonAn;
    priceController.text = widget.monAn.donGia.toString();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Detail', true, context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: ImageProduct(
                    url: "images/anh${widget.monAn.maMonAn}.jpg",
                    height: 180.0,
                    width: 180.0,
                  ),
                ),
                SizedBox(height: 20.0),
                Text('ID:', style: CommonTextStyle.size14Medium,),
                SizedBox(height: 8,),
                TextFieldOutLine(
                  isEnable: false,
                  controller: idController,
                ),
                SizedBox(height: 15,),
                Text('Name:', style: CommonTextStyle.size14Medium,),
                SizedBox(height: 8,),
                TextFieldOutLine(
                  controller: nameController,
                ),
                SizedBox(height: 15,),
                Text('Price:', style: CommonTextStyle.size14Medium,),
                SizedBox(height: 8,),
                TextFieldOutLine(
                  isEnable: true,
                  controller: priceController,
                ),
                SizedBox(height: 20,),
                Align(
                  alignment: Alignment.centerRight,
                  child: RaisedButton(
                    color: Colors.brown,
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    onPressed: () {
                      Map<String, dynamic> map = {
                        'maMonAn' : idController.text,
                        'tenMonAn' : nameController.text,
                        'donGia': priceController.text,
                        'loaiMon': {
                          "maLoaiMon": 4,
                        }
                      };
                      MonAnService.editMonAn(map).then((value) {
                        if (value.statusCode == 200) {
                          SnackbarBuilder.showSnackbar(content:'Success', color: Colors.blue);
                          print('edit');
                          Provider.of<MonAnProvider>(context, listen: false).getMonAn();
                          Navigator.pop(context);
                        } else {
                          SnackbarBuilder.showSnackbar(content:'Error', color: Colors.red);
                        }
                      });                    },
                    child: Text('Submit',),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
