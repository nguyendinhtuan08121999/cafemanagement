import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/screens/editmenu/widgets/edit-coffee-item.dart';
import 'package:cafe_management/screens/ordertable/widget/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EditProductRowItem extends StatelessWidget {
  const EditProductRowItem({
    this.index,
    this.product,
    this.lastItem,
  });

  final MonAn product;
  final int index;
  final bool lastItem;

  @override
  Widget build(BuildContext context) {
    final row = SafeArea(
      top: false,
      bottom: false,
      minimum: const EdgeInsets.only(
        left: 16,
        top: 8,
        bottom: 8,
        right: 8,
      ),
      child: InkWell(
        onTap: () {
          //Navigator.push(context, MaterialPageRoute(builder: (context) => EditCoffeeItem(product: product,)));
        },
        child: Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6)
              ),
              height: 67,
              width: 67,
              child: SvgPicture.asset('images/coffee-cup2.svg')
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      product.tenMonAn,
                      style: Styles.productRowItemName,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    Text(
                      '${product.donGia}',
                      style: Styles.productRowItemPrice,
                    ),
                  ],
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                print('delete');
              },
              icon: const Icon(Icons.cancel_outlined, color: Color(0xffca583e),),
            ),
          ],
        ),
      ),
    );

    // if (lastItem) {
    //   return row;
    // }

    return Column(
      children: <Widget>[
        row,
        Padding(
          padding: const EdgeInsets.only(
            left: 100,
            right: 16,
          ),
          child: Container(
            height: 1,
            color: Styles.productRowDivider,
          ),
        ),
      ],
    );
  }
}
