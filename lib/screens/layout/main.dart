import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/screens/editmenu/edit-menu.dart';
import 'package:cafe_management/screens/oderhistory/order-history.dart';
import 'package:cafe_management/screens/ordertable/order-table.dart';
import 'package:cafe_management/screens/profile/user-profile.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class DashboardHomePage extends StatefulWidget {
  DashboardHomePage({Key key}) : super(key: key);

  @override
  _DashboardHomePageState createState() => _DashboardHomePageState();
}

class _DashboardHomePageState extends State<DashboardHomePage> with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;
  String idBan = '';
  String trangThai = '';

  StompClient stompClient;
  List listPage = [ OrderTableScreen(), EditMenuScreen(), OrderHistoryScreen(), UserProfile()];
  @override
  void initState() {
    connectSocket();
    initializing();
    super.initState();
  }
  void _showNotifications() async {
    if (this.mounted) {
      await notification();
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }

  void initializing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        android: androidInitializationSettings, iOS: iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future<void> notification() async {
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.high,
        importance: Importance.max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(android: androidNotificationDetails, iOS: iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0, '', 'Bàn $idBan: $trangThai', notificationDetails);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      setState(() {
        _selectedIndex = 0;
      });
      debugPrint('Notification payload: $payload');
    }
  }

  void connectSocket() async {
    TokenObj token = await SharedPrefsService.getTokenObj();
    var stompClient = StompClient(
      config: StompConfig.SockJS(
        url: 'http://localhost:8080/notification',
        onConnect: onConnect,
        onWebSocketError: (dynamic error) => print(error.toString()),
        stompConnectHeaders: {'Authorization': 'Bearer ${token.accessToken}'},
        webSocketConnectHeaders: {'Authorization': 'Bearer ${token.accessToken}'},
      ),
    );
    stompClient.activate();
  }

  void onConnect(StompClient client, StompFrame frame) {
    client.subscribe(
      destination: '/notification/ping',
      callback: (StompFrame frame) {
        List<String> a = frame.body.split(" ");
        idBan = a[0];
        if (a[1] == 'ready') {
          trangThai = "vừa gọi món";
        } else if (a[1] == 'inuse') {
          trangThai = 'món ăn đã hoàn thành';
        } else trangThai = 'đã thanh toán';
        print(a[1]);
        _showNotifications();
        refresh();
      },
    );
  }

  Future<void> refresh() async {
    if (this.mounted) {
      Provider.of<MonAnProvider>(context, listen: false).getMonAn();
      Provider.of<TableProvider>(context, listen: false).getTable();
      Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listPage.elementAt(_selectedIndex),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-onclick.svg'),
              label: 'Order',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee-cup1.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-cup1-onclick.svg',),
              label: 'Edit Menu',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee-cup.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-cup-onclick.svg',),
              label: 'Order History',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/profile-user.svg', isActive: true),
              activeIcon: Padding(
                padding: const EdgeInsets.symmetric(vertical: 3,),
                child: SizedBox(
                  height: 24,
                  width: 24,
                  child: SvgPicture.asset(
                    'images/profile-user.svg',
                    color: Colors.brown,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              label: 'Profile',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.brown,
          unselectedItemColor: Theme.of(context).disabledColor,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          elevation: 1,
          showUnselectedLabels: true,
          selectedFontSize: 10,
          unselectedFontSize: 10,
          backgroundColor: Theme.of(context).backgroundColor,
        ),
      ),
    );
  }

  Widget buildIcon(String path, {bool isActive = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3,),
      child: SizedBox(
        height: 24,
        width: 24,
        child: SvgPicture.asset(
          path,
          color: isActive ? Theme.of(context).disabledColor : null,
          fit: BoxFit.contain,
        ),
      ),
    );
  }



  void _onItemTapped(int index) {
    setState(() {
      if (index == 1 && Provider.of<UserProvider>(context, listen: false).user.role == 'BOIBAN') {
        SnackbarBuilder.showSnackbar(content:'Bạn không có quyền', color: Colors.orange);
      } else
      _selectedIndex = index;
    });
  }
}
