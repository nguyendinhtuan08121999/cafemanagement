
import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/screens/oderhistory/widget/order-history-detail.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OrderHistoryScreen extends StatefulWidget {

  @override
  _TicketRequestScreenState createState() => _TicketRequestScreenState();
}

class _TicketRequestScreenState extends State<OrderHistoryScreen> {
  bool currentIndex = true;
  @override
  void initState() {
    Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppbarBuilder.primaryAppbar('Order History'),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = true;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                        child: Text('Paid', style: CommonTextStyle.size16Medium.copyWith(color: currentIndex? Colors.orange :Colors.grey),),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = false;
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          child: Text('UnPaid', style: CommonTextStyle.size16Medium.copyWith(color: !currentIndex? Colors.orange :Colors.grey),)),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Consumer<HoaDonProvider> (
                  builder: (context, model, child) {
                    print(model.hoaDonPaid.length.toString());
                    return model.load
                        ? Container(
                      height: 260,
                      child: Center(child: CircularProgressIndicator()),
                    )
                        : ListView.builder(
                      itemCount: currentIndex ? model.hoaDonPaid.length : model.hoaDonUnPaid.length,
                      itemBuilder: (context, index) {
                        return _buildItem(hd: currentIndex ? model.hoaDonPaid[index] : model.hoaDonUnPaid[index]);
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildItem({HoaDonGet hd}) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => OrderHistoryDetail(hd: hd,)));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 3.0,
              blurRadius: 5.0,
            )
          ],
          color: Colors.white,
        ),
        child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Order: ',
                      style: CommonTextStyle.size18Bold.copyWith(color:Color(0xFF575E67)),
                    ),
                    Text(
                      hd.maHoaDon.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: CommonTextStyle.size18Bold.copyWith(color:Color(0xFFCC8053)),
                    ),
                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                        'Created: ',
                        style: CommonTextStyle.size20Bold.copyWith(fontSize: 14, color:Color(0xFF575E67))
                    ),
                    Text(
                        '${DateFormat(Constant.formatDateHour1).format(hd.thoiGian)}',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.end,
                        style: CommonTextStyle.size20Bold.copyWith(fontSize: 12, color:Color(0xFF575E67))
                    ),
                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                        'Status: ',
                        style: CommonTextStyle.size14Bold.copyWith( color:Color(0xFF575E67))
                    ),
                    Text(
                        hd.daThanhToan ? 'Paid' : 'Wait',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.end,
                        style: CommonTextStyle.size20Bold.copyWith(
                            fontSize: 12,
                            color: hd.daThanhToan ? Colors.green : Colors.orange
                        )
                    ),
                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                        'Total: ',
                        style: CommonTextStyle.size16Bold.copyWith(color: Color(0xFF575E67))
                    ),
                    Text(
                        '${numberToMoneyString1(hd.tongTien)}',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: CommonTextStyle.size18Bold.copyWith(color: Colors.brown)
                    ),
                  ],
                ),
              ],
            )
        ),
      ),
    );
  }

  String numberToMoneyString1(double price, {String unit = 'đ'}) {
    return NumberFormat("#,###", "vi_VN").format(price ?? 0) + unit;
  }

  String numberToMoneyString(int price, {String unit = 'đ'}) {
    return NumberFormat("#,###", "vi_VN").format(price) + unit;
  }
}

