import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/screens/oderhistory/widget/pdf_api.dart';
import 'package:cafe_management/screens/oderhistory/widget/pdf_invoice_api.dart';
import 'package:cafe_management/screens/ordertable/widget/loai-mon.dart';
import 'package:cafe_management/screens/ordertable/widget/menu.dart';
import 'package:cafe_management/screens/ordertable/widget/styles.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:cafe_management/services/table-service.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/dialog-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OrderHistoryDetail extends StatelessWidget {
  final HoaDonGet hd;
  OrderHistoryDetail({this.hd});

  @override
  Widget build(BuildContext context) {
    List<ListChiTietHoaDon> liCTHD = hd.listChiTietHoaDon;
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Order: ${hd.maHoaDon}', true, context),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20,),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Table',
                        style: CommonTextStyle.size16Medium,
                      ),
                      Text(
                        '${hd.ban.maBan}',
                        style: CommonTextStyle.size16Bold,
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Employee',
                        style: CommonTextStyle.size16Medium,
                      ),
                      Text(
                        hd.tenNhanVien,
                        style: CommonTextStyle.size16Medium,
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Create At',
                        style: CommonTextStyle.size16Medium,
                      ),
                      Text(
                        '${DateFormat(Constant.formatDateHour1).format(hd.thoiGian)}',
                        style: CommonTextStyle.size16Medium,
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Status',
                        style: CommonTextStyle.size16Medium,
                      ),
                      hd.daThanhToan
                      ? Text(
                        'Paid',
                        style: CommonTextStyle.size16Medium.copyWith(color: Colors.blue),
                      )
                      : (hd.ban.tinhTrang == 'inuse'
                          ? Text(
                            'Ready',
                            style: CommonTextStyle.size16Medium.copyWith(color: Colors.red),
                          )
                          : Text(
                            'Waiting',
                            style: CommonTextStyle.size16Medium.copyWith(color: Colors.orange),
                           )
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'List order',
                        style: CommonTextStyle.size16Bold,
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return itemCTHD(liCTHD[index]);
                    },
                    itemCount: liCTHD.length
                  ),
                  //SizedBox(height: 10,),
                  !hd.daThanhToan
                  ? RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 45, vertical: 10),
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.brown, width: 2),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    elevation: 0,
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => LoaiMonScreen(table: hd.ban, hd: hd,)));
                    },
                    color: Colors.white,
                    child: Text('Add more', style: TextStyle(color: Colors.brown),),
                  )
                  : SizedBox(),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Total',
                        style: CommonTextStyle.size16Bold,
                      ),
                      Text(
                        '${numberToMoneyString(hd.tongTien)}',
                        style: CommonTextStyle.size16Bold.copyWith(fontSize: 25, color: Color(0xff895141),)
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    color: Colors.brown.withOpacity(0.8),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                    onPressed: () async{
                      final pdfFile = await PdfInvoiceApi.generate(hd);

                      PdfApi.openFile(pdfFile);
                    },
                    child: Text('Export'),
                  ),
                  !hd.daThanhToan
                  ? (hd.ban.tinhTrang == 'ready'
                    ? RaisedButton(
                      onPressed: () {
                        if (Provider.of<UserProvider>(context, listen: false).user.role == 'BOIBAN') {
                          SnackbarBuilder.showSnackbar(content:'Bạn không có quyền', color: Colors.orange);
                        } else {
                        Map<String, dynamic> mapBan = {
                          'maBan' : hd.ban.maBan,
                          'ghiChu' : hd.ban.ghiChu,
                          'tinhTrang': 'inuse',
                          'moTa': hd.ban.moTa
                        };
                        TableService.editBan(mapBan).then((value){
                          Provider.of<TableProvider>(context, listen: false).getTable();
                          Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
                          SnackbarBuilder.showSnackbar(content:'Success', color: Colors.blue);
                          Navigator.pop(context);
                        });
                      }},
                      child: Text('Sẵn sàng'),
                    )
                    : RaisedButton(
                    color: Colors.brown.withOpacity(0.8),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                    onPressed: () {
                      DialogBuilder.buildLoadingDialog();
                      Map<String, dynamic> mapBan = {
                        'maBan' : hd.ban.maBan,
                        'ghiChu' : hd.ban.ghiChu,
                        'tinhTrang': 'blank',
                        'moTa': hd.ban.moTa,
                      };
                      Map<String, dynamic> mapHD1 = {
                        'maHoaDon' : hd.maHoaDon,
                        'ban': {
                          'maBan': hd.ban.maBan,
                        },
                        'tenNhanVien': hd.tenNhanVien,
                        'thoiGian': hd.thoiGian.toIso8601String(),
                        'tongTien': hd.tongTien,
                        'daThanhToan': true,
                        'listChiTietHoaDon': List<dynamic>.from(hd.listChiTietHoaDon.map((x) => x.toJson())),
                      };
                      TableService.editBan(mapBan).then((value) {
                        Provider.of<TableProvider>(context, listen: false).getTable();
                        MonAnService.editHoaDon(mapHD1).then((value) {
                          Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
                          Navigator.pop(context);
                          Navigator.pop(context);
                        });
                        SnackbarBuilder.showSnackbar(content:'Success', color: Colors.blue);
                      });
                    },
                    child: Text('Thanh toán'),
                  )
                  )
                  : SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget itemCTHD(ListChiTietHoaDon ct) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6)
          ),
          child: ImageProduct(
            url: "images/anh${ct.monAn.maMonAn}.jpg",
            height: 50.0,
            width: 50.0,
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8, top: 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      ct.monAn.tenMonAn,
                      style: Styles.productRowItemName,
                    ),
                    Text(
                      '${numberToMoneyString(ct.donGia)}',
                      style: Styles.productRowItemName,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  '${ct.soLuong > 1 ? '${ct.soLuong} x ' : ''}'
                      '${numberToMoneyString(ct.donGia/ct.soLuong)}',
                  style: Styles.productRowItemPrice,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  String numberToMoneyString(double price, {String unit = 'đ'}) {
    return NumberFormat("#,###", "vi_VN").format(price) + unit;
  }
}
