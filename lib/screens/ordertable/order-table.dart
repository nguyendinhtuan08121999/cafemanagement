import 'dart:convert';

import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/screens/oderhistory/widget/order-history-detail.dart';
import 'package:cafe_management/screens/ordertable/widget/item_card.dart';
import 'package:cafe_management/screens/ordertable/widget/loai-mon.dart';
import 'package:cafe_management/screens/ordertable/widget/menu.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/shared-preference.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/item-product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';


class OrderTableScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<OrderTableScreen> {

  @override
  void initState() {
    if(mounted) {
      setState(() {
        Provider.of<MonAnProvider>(context, listen: false).getMonAn();
        Provider.of<TableProvider>(context, listen: false).getTable();
        Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
      });
    }
    super.initState();
  }


  Future<void> refresh() async {
    if (this.mounted) {
      Provider.of<MonAnProvider>(context, listen: false).getMonAn();
      Provider.of<TableProvider>(context, listen: false).getTable();
      Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Đặt bàn'),
      body: SafeArea(
        child:  RefreshIndicator(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, left: 20,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Bán chạy',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 30,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 10,),
                    Consumer<MonAnProvider>(
                        builder: (context, model, child) {
                          return model.load
                              ? Container(
                            height: 260,
                            child: Center(child: CircularProgressIndicator()),
                          )
                              : SizedBox(
                            height: 185,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: model.liMonAn.length,
                              itemBuilder: (context, index) {
                                return ItemProduct(monAn: model.liMonAn[index],);
                              },
                            ),
                          );
                        }
                    ),
                    SizedBox(height: 20,),
                    Text(
                      'Chọn bàn',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 30,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 10,),
                    Consumer<TableProvider>(
                      builder: (context, model, child) {
                        return model.load
                            ? Container(
                          height: 260,
                          child: Center(child: CircularProgressIndicator()),
                        )
                            : Container(
                          height: 500,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: model.litable.length,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  mainAxisSpacing: 10,
                                  crossAxisSpacing: 20,
                                  childAspectRatio: 4,
                                ),
                                itemBuilder: (context, index) => ItemCard(
                                    table: model.litable[index],
                                    press: () {
                                      // ignore: unrelated_type_equality_checks
                                      if (model.litable[index].tinhTrang == 'blank') {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => LoaiMonScreen(table: model.litable[index],)));
                                      } else {
                                        HoaDonGet hd = Provider.of<HoaDonProvider>(context, listen: false).getByTable(maBan: model.litable[index].maBan);
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => OrderHistoryDetail(hd: hd,)));
                                      }
                                    }
                                )
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
          onRefresh: refresh,
        ),
      ),
    );
  }
}
