import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ItemCard extends StatelessWidget {
  final TableCoffee table;
  final Function press;
  const ItemCard({
    Key key,
    this.table,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 15.0, right: 5.0),
        height: 30,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 3.0,
              blurRadius: 5.0
            )
          ],
          color: Colors.white
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Bàn ${table.maBan}',
              style:table.tinhTrang == 'blank'
                ? CommonTextStyle.size14Bold
                : table.tinhTrang == 'ready'? CommonTextStyle.size14Bold.copyWith(color: Colors.orange):CommonTextStyle.size14Bold.copyWith(color: Colors.red)
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: table.tinhTrang == 'blank'
                ? Colors.black
                : (table.tinhTrang == 'ready'?Colors.orange:Colors.red)
            ),
          ],
        ),
      ),
    );
  }
}
