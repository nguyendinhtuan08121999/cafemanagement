import 'dart:ui';

import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:flutter/material.dart';

import 'menu.dart';
class LoaiMonScreen extends StatelessWidget {
  final TableCoffee table;
  final HoaDonGet hd;

  LoaiMonScreen({this.table, this.hd});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Loại món',true,context),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
            _buildLoaiMon(context: context, title: 'Cà phê', index: 1, img: 'images/cate1.jpeg', color: Colors.white),
            const SizedBox(height: 30,),
            _buildLoaiMon(context: context, title:'Trà sữa', index: 2, img: 'images/cate.jpeg', color: Colors.white),
            const SizedBox(height: 30,),
            _buildLoaiMon(context: context, title:'Đặc biệt', index: 3, img: 'images/cate3.jpeg', color: Colors.white),
            const SizedBox(height: 30,),
          ],
        )
      ),
    );
  }

  Widget _buildLoaiMon({BuildContext context, String title, int index, String img, Color color}) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => MenuHome(table: table, index: index, hd: hd,)));
      },
      child: Stack(
        children: [
          ImageProduct(
            url: img,
            height: 110,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3.5, sigmaY: 3.5),
              child: Container(
                //padding: EdgeInsets.only(top: 20),
                height: 110,
                color: Colors.grey.withOpacity(0.1),
                alignment: Alignment.center,
                child: Text(title, style: TextStyle(color: color, fontWeight: FontWeight.w700, fontSize: 24),),
              ),
            ),
          )
        ],
      ),
    );
  }
}
