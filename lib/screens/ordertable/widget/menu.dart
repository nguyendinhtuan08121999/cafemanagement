import 'package:badges/badges.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/screens/ordertable/widget/monan-detail.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/widgets/item-product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MenuHome extends StatefulWidget {
  final TableCoffee table;
  final HoaDonGet hd;
  final int index;

  MenuHome({this.table, this.hd, this.index});
  @override
  _MenuHomeState createState() => _MenuHomeState();
}

class _MenuHomeState extends State<MenuHome> {

  void initState() {
    Provider.of<MonAnProvider>(context, listen: false).getMonAn();
    print('index: ${widget.index}');
    print('listMA1: ${Provider.of<MonAnProvider>(context, listen: false).liMonAn1.length}');
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<MonAnProvider>(
      builder: (context, model, child){
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => ShoppingCartTab(table: widget.table, hd: widget.hd,)));
            },
            child: Badge(
              badgeContent: Text(
                '${Provider.of<MonAnProvider>(context, listen: false).productsInCart.length}',
                style: TextStyle(color: Colors.white),),
              child: Icon(Icons.shopping_cart)
            ),
            backgroundColor: Colors.brown,
          ),
          appBar: AppbarBuilder.primaryAppbar('Menu', true, context),
          body: Padding(
            padding: const EdgeInsets.only(top: 20, left: 35, right: 30),
            child: check(),
          ),
        );
      }
    );
  }

  Widget check() {
    switch (widget.index) {
      case 1:
        return monan1();
      case 2:
        return monan2();
      case 3:
        return monan3();
    }
  }

  Widget monan1() {
    return Consumer<MonAnProvider>(
      builder: (context, model, child) {
        return GridView.builder(
            itemCount: model.liMonAn1.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 50,
              childAspectRatio: 0.80,
            ),
            itemBuilder: (context, index) => ItemProduct(
              monAn: model.liMonAn1[index],
              press: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailMon(monan: model.liMonAn1[index],)));
              },
            )
        );
      },
    );
  }

  Widget monan2() {
    return Consumer<MonAnProvider>(
      builder: (context, model, child) {
        return GridView.builder(
            itemCount: model.liMonAn2.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 50,
              childAspectRatio: 0.83,
            ),
            itemBuilder: (context, index) => ItemProduct(
              monAn: model.liMonAn2[index],
              press: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailMon(monan: model.liMonAn2[index],)));
              },
            )
        );
      },
    );
  }

  Widget monan3() {
    return Consumer<MonAnProvider>(
      builder: (context, model, child) {
        return GridView.builder(
            itemCount: model.liMonAn3.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 50,
              childAspectRatio: 0.83,
            ),
            itemBuilder: (context, index) => ItemProduct(
              monAn: model.liMonAn3[index],
              press: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailMon(monan: model.liMonAn3[index],)));
              },
            )
        );
      },
    );
  }

  Widget _buildCard({MonAn monAn, bool isEdit = false}) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailMon(monan: monAn,)));
      },
      child: Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: Container(
          padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
          width: 150,
          height: 300,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3.0,
                blurRadius: 5.0
              )
            ],
            color: Colors.white,
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                      Icon(Icons.favorite_border_outlined, color: Color(0xFFEF7532))
                  ]
                )
              ),
              Container(
                height: 75.0,
                width: 75.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("images/coffeesplash3.png"),
                    fit: BoxFit.contain,
                  )
                )
              ),
              SizedBox(height: 7.0),
              Text(numberToMoneyString1(monAn.donGia),
                style: TextStyle(
                  color: Color(0xFFCC8053),
                  fontFamily: 'Varela',
                  fontSize: 14.0)),
              Text(monAn.tenMonAn,
                style: TextStyle(
                  color: Color(0xFF575E67),
                  fontFamily: 'Varela',
                  fontSize: 14.0)),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(color: Color(0xFFEBEBEB), height: 1.0)),
              Padding(
                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      if (!isEdit) ...[
                        Icon(Icons.shopping_basket,
                          color: Color(0xFFD17E50), size: 12.0),
                        Text('Add to cart',
                          style: TextStyle(
                            fontFamily: 'Varela',
                            color: Color(0xFFD17E50),
                            fontSize: 12.0
                          )
                        )
                      ],
                      if (isEdit) ...[
                        Icon(Icons.remove_circle_outline,
                            color: Color(0xFFD17E50), size: 12.0),
                        Text('3',
                            style: TextStyle(
                                fontFamily: 'Varela',
                                color: Color(0xFFD17E50),
                                fontWeight: FontWeight.bold,
                                fontSize: 12.0
                            )
                        ),
                        Icon(Icons.add_circle_outline,
                            color: Color(0xFFD17E50), size: 12.0
                        ),
                      ]
                    ]
                  )
              )
            ]
          )
        )
      ),
    );
  }
}
