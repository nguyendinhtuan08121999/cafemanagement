import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/widgets/defauld-button.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailMon extends StatefulWidget {
  final MonAn monan;

  DetailMon({this.monan});

  @override
  _DetailMonState createState() => _DetailMonState();
}

class _DetailMonState extends State<DetailMon> {
  int soluong = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Pickup', true, context),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
            children: [
              SizedBox(height: 15.0),
              // Text(
              //   'Coffee',
              //   style: TextStyle(
              //     fontFamily: 'Varela',
              //     fontSize: 42.0,
              //     fontWeight: FontWeight.bold,
              //     color: Colors.brown)
              // ),
              SizedBox(height: 15.0),
              ImageProduct(
                url: "images/anh${widget.monan.maMonAn}.jpg",
                height: 180.0,
                width: 180.0,
              ),
              SizedBox(height: 20.0),
              Center(
                child: Text('${numberToMoneyString1(widget.monan.donGia)}đ',
                  style: TextStyle(
                    fontFamily: 'Varela',
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.brown
                  )
                ),
              ),
              SizedBox(height: 10.0),
              Center(
                child: Text(widget.monan.tenMonAn,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Color(0xFF575E67),
                    fontFamily: 'Varela',
                    fontSize: 24.0,
                  )
                ),
              ),
              SizedBox(height: 20.0),
              Center(
                child: Container(
                  width: MediaQuery.of(context).size.width - 50.0,
                  child: Text('Cold, creamy ice cream sandwiched between delicious deluxe cookies. Pick your favorite deluxe cookies and ice cream flavor.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Varela',
                      fontSize: 16.0,
                      color: Color(0xFFB4B8B9))
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              buildQuantity(),
              SizedBox(height: 20.0),
              DefaultButton(
                text: 'Add to cart',
                press: () {
                  final model = Provider.of<MonAnProvider>(context, listen: false);
                  model.addProductToCart(widget.monan.maMonAn, soluong);
                  Navigator.pop(context);
                },
              )
            ]
        ),
      ),
    );
  }

  Widget buildQuantity() {
    final style = TextStyle(fontWeight: FontWeight.bold, fontSize: 22);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.black26),
          ),
          child: MaterialButton(
            minWidth: 60,
            child: Text('—', style: style),
            onPressed: () {
              if (soluong != 1){
                setState(() {
                  soluong -- ;
                });
              }
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(soluong.toString(), style: style),
        ),
        SizedBox(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.black26),
            ),
            child: MaterialButton(
              minWidth: 60,
              child: Text('+', style: style.copyWith(fontSize: 24)),
              onPressed: () {
                setState(() {
                  soluong ++;
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}
