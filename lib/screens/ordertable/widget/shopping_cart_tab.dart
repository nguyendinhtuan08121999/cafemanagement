// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/hoadon/hoadon.dart';
import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/monan/monan-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/router/routing-name.dart';
import 'package:cafe_management/services/monan-service.dart';
import 'package:cafe_management/services/table-service.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/dialog-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'styles.dart';

class ShoppingCartTab extends StatefulWidget {
  final TableCoffee table;
  final HoaDonGet hd;
  ShoppingCartTab({this.table, this.hd});
  @override
  _ShoppingCartTabState createState() {
    return _ShoppingCartTabState();
  }
}

class _ShoppingCartTabState extends State<ShoppingCartTab> {
  DateTime dateTime = DateTime.now();

  SliverChildBuilderDelegate _buildSliverChildBuilderDelegate(
      MonAnProvider model) {
    return SliverChildBuilderDelegate(
      (context, index) {
        final productIndex = index -4;
        switch (index) {
          case 0:
            return SizedBox(
            );
          case 1:
            return SizedBox(

            );
          case 2:
            return SizedBox(

            );
          case 3:
            return SizedBox(

            );
          default:
            if (model.productsInCart.length > productIndex) {
              return ShoppingCartItem(
                index: index,
                product: model.getProductById(
                    model.productsInCart.keys.toList()[productIndex]),
                quantity: model.productsInCart.values.toList()[productIndex],
                lastItem: productIndex == model.productsInCart.length - 1,
              );
            } else if (model.productsInCart.keys.length == productIndex &&
                model.productsInCart.isNotEmpty) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        const SizedBox(height: 6),
                        Text(
                          'Total  ${numberToMoneyString1(model.subtotalCost)}đ',
                          style: Styles.productRowTotal,
                        ),
                        const SizedBox(height: 15),
                        RaisedButton(
                          color: Colors.brown,
                          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                          onPressed: () async {
                            DialogBuilder.buildLoadingDialog();
                            Ban ban = new Ban();
                            ban.maBan = widget.table.maBan;
                            Map _formdata = {
                              "ban": ban.toJson(),
                              "thoiGian": DateTime.now().toIso8601String(),
                              "tongTien": model.subtotalCost,
                              "daThanhToan": false,
                              "tenNhanVien": Provider.of<UserProvider>(context, listen: false).user.username,
                              "listChiTietHoaDon": model.listCTHD(),
                            };
                            Map<String, dynamic> mapBan = {
                              'maBan' : widget.table.maBan,
                              'ghiChu' : widget.table.ghiChu,
                              'tinhTrang': 'ready',
                              'moTa': widget.table.moTa
                            };

                            if (widget.table.tinhTrang != 'inuse' && widget.table.tinhTrang != 'ready') {
                              MonAnService.addHoaDon(_formdata).then((value) {
                                if (value.statusCode == 200) {
                                  Provider.of<MonAnProvider>(context, listen: false).clearCart();
                                  TableService.editBan(mapBan).then((value) {
                                    Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
                                    Provider.of<TableProvider>(context, listen: false).getTable();
                                    //SnackbarBuilder.showSnackbar(content:'Success', color: Colors.blue);
                                    Navigator.pop(context);
                                    Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.homeRoute, (route) => false);
                                  });
                                } else {
                                  SnackbarBuilder.showSnackbar(content:'Error', color: Colors.red);
                                }
                              });
                            } else {
                              List<dynamic> li =  List<dynamic>.from(widget.hd.listChiTietHoaDon.map((x) => x.toJson()));
                              li.addAll(model.listCTHD());
                              Map _formdata2 = {
                                'maHoaDon' : widget.hd.maHoaDon,
                                "ban": widget.hd.ban,
                                "thoiGian": widget.hd.thoiGian.toIso8601String(),
                                "tongTien": widget.hd.tongTien + model.subtotalCost,
                                "daThanhToan": false,
                                "tenNhanVien": widget.hd.tenNhanVien,
                                "listChiTietHoaDon": li,
                              };
                              MonAnService.editHoaDon(_formdata2).then((value) {
                                if(value.statusCode == 200){
                                  Provider.of<MonAnProvider>(context, listen: false).clearCart();
                                  TableService.editBan(mapBan).then((value) {
                                    Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
                                    SnackbarBuilder.showSnackbar(content: 'Success', color: Colors.blue);
                                    Navigator.pop(context);
                                    Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.homeRoute, (route) => false);
                                  });

                                  // Navigator.pop(context);
                                }
                              });
                            }
                          },
                          child: Text('Submit',),
                        )
                      ],
                    )
                  ],
                ),
              );
            }
        }
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Cart', true, context),
      body: Consumer<MonAnProvider>(
        builder: (context, model, child) {
          return CustomScrollView(
            slivers: <Widget>[
              SliverSafeArea(
                top: false,
                minimum: const EdgeInsets.only(top: 4),
                sliver: SliverList(
                  delegate: _buildSliverChildBuilderDelegate(model),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class ShoppingCartItem extends StatelessWidget {
  const ShoppingCartItem({
    @required this.index,
    @required this.product,
    @required this.lastItem,
    @required this.quantity,
  });

  final MonAn product;
  final int index;
  final bool lastItem;
  final int quantity;

  @override
  Widget build(BuildContext context) {
    final row = SafeArea(
      top: false,
      bottom: false,
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
        ),
        padding: const EdgeInsets.only(
          left: 16,
          top: 8,
          bottom: 8,
          right: 8,
        ),
        child: Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6)
              ),
              child: ImageProduct(
                url: "images/anh${product.maMonAn}.jpg",
                height: 50.0,
                width: 50.0,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          product.tenMonAn,
                          style: Styles.productRowItemName,
                        ),

                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      '${numberToMoneyString(quantity * product.donGia)}',
                      style: CommonTextStyle.size18Bold.copyWith(color: Colors.brown),
                    ),
                  ],
                ),
              ),
            ),

            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                final model = Provider.of<MonAnProvider>(context, listen: false);
                model.removeItemFromCart(product.maMonAn);
              },
              icon: const Icon(Icons.remove_circle_outline, color: Color(0xffCA583E),),
            ),
            Text('$quantity', style: CommonTextStyle.size16Medium,),
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                final model = Provider.of<MonAnProvider>(context, listen: false);
                model.addProductToCart(product.maMonAn, 1);
              },
              icon: const Icon(Icons.add_circle_outline, color: Colors.blueAccent,),
            ),
          ],
        ),
      ),
    );

    return row;
  }

  String numberToMoneyString(double price, {String unit = 'đ'}) {
    return NumberFormat("#,###", "vi_VN").format(price) + unit;
  }
}
String numberToMoneyString1(double price, {String unit = 'đ'}) {
  return NumberFormat("#,###", "vi_VN").format(price);
}
