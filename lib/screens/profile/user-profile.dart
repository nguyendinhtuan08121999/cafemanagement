import 'package:cafe_management/models/user/user.dart';
import 'package:cafe_management/providers/common/token-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/router/routing-name.dart';
import 'package:cafe_management/screens/profile/widget/profile-item-menu.dart';
import 'package:cafe_management/screens/profile/widget/thong-ke.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/utils/dialog-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Profile'),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 25, left: 20, right: 20, bottom: 25),
          child: Column(
            children: [
              Align(
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: Consumer<UserProvider>(
                    builder: (context, model, child) {
                      User user = model.user;
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 25,),
                          SizedBox(height: 30),
                          ProfileMenuItem(
                            icon: 'images/ic_nameprofile.svg',
                            title: '${user?.displayName}',
                          ),
                          ProfileMenuItem(
                            icon: 'images/ic_username.svg',
                            title: '${user?.username}',
                          ),
                          ProfileMenuItem(
                            icon: 'images/ic_role.svg',
                            title: '${user?.role}',
                            isLast: true,
                          ),

                          const SizedBox(height: 25,),
                        ],
                      );
                    },
                  )),
              ),
              GestureDetector(
                child: Container(
                  margin: const EdgeInsets.only(top: 15),
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15,),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(width: 20,),
                      SizedBox(
                        height: 24,
                        width: 24,
                        child: SvgPicture.asset(
                          'images/bar-chart.svg',
                          fit: BoxFit.contain,
                          color: Theme.of(context).disabledColor,
                        ),
                      ),
                      const SizedBox(width: 20,),
                      Text(
                        'Statistical',
                        style: CommonTextStyle.size14Light,
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if (Provider.of<UserProvider>(context, listen: false).user.role == 'BOIBAN') {
                    SnackbarBuilder.showSnackbar(content:'Bạn không có quyền', color: Colors.orange);
                  } else
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ThongKeScreen()));
                },
              ),
              GestureDetector(
                child: Container(
                  margin: const EdgeInsets.only(top: 15),
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15,),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(width: 20,),
                      SizedBox(
                        height: 24,
                        width: 24,
                        child: SvgPicture.asset(
                          'images/ic_logout.svg',
                          fit: BoxFit.contain,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                      const SizedBox(width: 20,),
                      Text(
                        'Logout',
                        style: CommonTextStyle.size14Medium.copyWith(
                          color: Theme.of(context).accentColor,
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  DialogBuilder.buildConfirmDialog(
                    context: context,
                    message: 'Do you really want to log out',
                    imageSvg: 'images/ic_logout_circle.svg',
                  ).then((value){
                    if (value){
                      Provider.of<TokenProvider>(context, listen: false).removeToken();
                      Provider.of<UserProvider>(context, listen: false).removeUser();
                      Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.splashScreen, (route) => false);
                    }
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
