import 'dart:io';
import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/thongke/thongke-all.dart';
import 'package:cafe_management/models/thongke/thongke.dart';
import 'package:cafe_management/screens/oderhistory/widget/pdf_api.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/widgets.dart';

class PdfThongKe {
  static Future<File> generate(ThongKeAll invoice, DateTime start, DateTime end) async {
    final pdf = Document();

    pdf.addPage(MultiPage(
      build: (context) => [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'CUPERTINO STORE',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 1 * PdfPageFormat.cm),
            buildText(
              title: 'Start date: ',
              value: '${DateFormat(Constant.formatDate).format(start)}'
            ),
            buildText(
              title: 'Table: ',
              value: '${DateFormat(Constant.formatDate).format(end)}'
            ),
            buildText(
              title: 'Number of bill: ',
              value: ''
            ),
            buildText(
              title: 'Number of cup: ',
              value: ''
            ),
            buildText(
              title: 'Total sale: ',
              value: ''
            ),
          ],
        ),
      ],
      footer: (context) => buildFooter(),
    ));

    return PdfApi.saveDocument(name: 'ThongKe.pdf', pdf: pdf);
  }


  static Widget buildInvoice(HoaDonGet invoice) {
    final headers = [
      'Name',
      'Amount',
      'Quantity',
      'Total',
    ];
    final data = invoice.listChiTietHoaDon.map((item) {

      return [
        item.monAn.tenMonAn,
        '${numberToMoneyString1(item.monAn.donGia)}',
        item.soLuong,
        '${numberToMoneyString1(item.donGia)}',
      ];
    }).toList();

    return Table.fromTextArray(
      headers: headers,
      data: data,
      border: null,
      headerStyle: TextStyle(fontWeight: FontWeight.bold),
      headerDecoration: BoxDecoration(color: PdfColors.grey300),
      cellHeight: 30,
      cellAlignments: {
        0: Alignment.centerLeft,
        1: Alignment.centerRight,
        2: Alignment.centerRight,
        3: Alignment.centerRight,
      },
    );
  }

  static Widget buildTotal(HoaDonGet invoice) {
    final total = invoice.tongTien;

    return Container(
      alignment: Alignment.centerRight,
      child: Row(
        children: [
          Spacer(flex: 6),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildText(
                  title: 'Total amount due',
                  titleStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                  value: '${numberToMoneyString1(total)} ',
                  unite: true,
                ),
                SizedBox(height: 2 * PdfPageFormat.mm),
                Container(height: 1, color: PdfColors.grey400),
                SizedBox(height: 0.5 * PdfPageFormat.mm),
                Container(height: 1, color: PdfColors.grey400),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static Widget buildFooter() => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Divider(),
          SizedBox(height: 2 * PdfPageFormat.mm),
          buildSimpleText(title: 'Address', value: 'Cau Dien Street, Ha Noi'),
          SizedBox(height: 1 * PdfPageFormat.mm),
        ],
      );

  static buildSimpleText({
    String title,
    String value,
  }) {
    final style = TextStyle(fontWeight: FontWeight.bold);

    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: pw.CrossAxisAlignment.end,
      children: [
        Text(title, style: style),
        SizedBox(width: 2 * PdfPageFormat.mm),
        Text(value),
      ],
    );
  }

  static buildText({
    String title,
    String value,
    double width = double.infinity,
    TextStyle titleStyle,
    bool unite = false,
  }) {
    final style = titleStyle ?? TextStyle(fontWeight: FontWeight.bold);

    return Container(
      width: width,
      child: Row(
        children: [
          Expanded(child: Text(title, style: style)),
          Text(value, style: unite ? style : null),
        ],
      ),
    );
  }
}
