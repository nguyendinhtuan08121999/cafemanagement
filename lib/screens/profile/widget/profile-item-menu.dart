import 'package:cafe_management/styles/component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    Key key,
    this.icon,
    this.title,
    this.onTap,
    this.isLast = false,
  }) : super(key: key);

  final String icon, title;
  final Function onTap;
  final bool isLast;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 10,),
          child: Row(
            children: [
              SizedBox(
                child: SvgPicture.asset(
                  icon,
                  fit: BoxFit.contain,
                  color: Theme.of(context).disabledColor,
                  height: 24,
                  width: 24,
                ),
              ),
              const SizedBox(width: 20,),
              Expanded(
                child: Text(title ?? '--', style: CommonTextStyle.size14Light),
              ),
            ],
          ),
        ),
        !isLast 
        ? Divider(color: Theme.of(context).disabledColor)
        : SizedBox()
      ],
    );
  }
}
