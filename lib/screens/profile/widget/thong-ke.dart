import 'dart:async';

import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/thongke/thongke-all.dart';
import 'package:cafe_management/models/thongke/thongke.dart';
import 'package:cafe_management/screens/oderhistory/widget/pdf_api.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:cafe_management/screens/ordertable/widget/styles.dart';
import 'package:cafe_management/screens/profile/widget/pdf_invoice.dart';
import 'package:cafe_management/services/thongke-service.dart';
import 'package:cafe_management/styles/component.dart';
import 'package:cafe_management/utils/appbar-builder.dart';
import 'package:cafe_management/widgets/image/image-product.dart';
import 'package:cafe_management/widgets/textfield-outline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';


class ThongKeScreen extends StatefulWidget {
  @override
  _ThongKeState createState() => _ThongKeState();
}

class _ThongKeState extends State<ThongKeScreen> {
  TextEditingController controllerStart = new TextEditingController();
  TextEditingController controllerEnd = new TextEditingController();
  ThongKeAll thongKe = ThongKeAll(
    doanhSoCuaNhanVien: [],
    from: '',
    to:'',
    soLuongMon: [],
  );
  DateTime start;
  DateTime end;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarBuilder.primaryAppbar('Statistical', true, context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text('Start Date:', style: CommonTextStyle.size12Light,),
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      child: Text('End Date:', style: CommonTextStyle.size12Light,),
                    )
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(context,
                            theme: DatePickerTheme(
                              containerHeight: 210.0,
                            ),
                            showTitleActions: true,
                            //minTime: startDay,
                            // maxTime: checkTimeEnd,
                            onConfirm: (date) {
                              setState(() {
                                start = DateTime(date.year, date.month, date.day, 00,00,00);
                                print('start: ${start.toString()}');
                                controllerStart.text = new DateFormat(Constant.formatDate).format(date);
                              });
                            }, currentTime: DateTime.now(), locale: LocaleType.vi,
                          );
                        },
                        child: SizedBox(
                          height: 45,
                          child: TextFieldOutLine(
                            controller: controllerStart,
                            isEnable: false,
                            suffixIcon: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10,),
                              child: SvgPicture.asset(
                                'images/ic_calendar.svg',
                                color: Colors.brown.withOpacity(0.68),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(context,
                            theme: DatePickerTheme(
                              containerHeight: 210.0,
                            ),
                            showTitleActions: true,
                            minTime: start,
                            // maxTime: checkTimeEnd,
                            onConfirm: (date) {
                              setState(() {
                                end = DateTime(date.year, date.month, date.day, 23,59,59);
                                print('start: ${end.toString()}');
                                controllerEnd.text = new DateFormat(Constant.formatDate).format(date);
                              });
                            }, currentTime: DateTime.now(), locale: LocaleType.vi,
                          );
                        },
                        child: SizedBox(
                          height: 45,
                          child: TextFieldOutLine(
                            controller: controllerEnd,
                            isEnable: false,
                            suffixIcon: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10,),
                              child: SvgPicture.asset(
                                'images/ic_calendar.svg',
                                color: Colors.brown.withOpacity(0.68),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10,),
                Align(
                  alignment: Alignment.center,
                  child: RaisedButton(
                    color: Colors.brown.withOpacity(0.8),
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                    onPressed: () {
                      ThongKeService.getAllThongKe(start, end).then((value) {
                        if (value != null)
                          thongKe = value;
                        setState(() {});
                      });
                    },
                    child: Text('Submit', style: TextStyle(color: Colors.white),),
                  ),
                ),
                const SizedBox(height: 20,),
                Text(
                  'Thống kê theo nhân viên:',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.brown,
                  ),
                ),
                const SizedBox(height: 10,),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: thongKe.doanhSoCuaNhanVien.length,
                  itemBuilder: (context, index) {
                    return _buildNhanVienItem(thongKe.doanhSoCuaNhanVien[index]);
                  }
                ),
                const SizedBox(height: 10,),
                Text(
                  'Thống kê món ăn:',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.brown,
                  ),
                ),
                const SizedBox(height: 10,),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: thongKe.soLuongMon.length,
                  itemBuilder: (context, index) {
                    return _buildMonAn(thongKe.soLuongMon[index]);
                  }
                )
              ],
            ),
          ),
        )
      ),
    );
  }

  Widget _buildMonAn(SoLuongMon mon) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
      ),
      padding: const EdgeInsets.only(
        left: 16,
        top: 8,
        bottom: 8,
        right: 8,
      ),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6)
            ),
            child: ImageProduct(
              url: "images/anh${mon.maMonAn}.jpg",
              height: 50.0,
              width: 50.0,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        mon.tenMonAn,
                        style: Styles.productRowItemName,
                      ),

                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Text(
                        'Số lượng:',
                        style: CommonTextStyle.size14Medium,
                      ),
                      const SizedBox(width: 25,),
                      Text(
                        mon.soLuong.toString(),
                        style: CommonTextStyle.size18Bold.copyWith(color: Colors.brown),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNhanVienItem(DoanhSoCuaNhanVien nv) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 3.0,
            blurRadius: 5.0,
          )
        ],
        color: Colors.white,
      ),
      child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Tài khoản: ',
                    style: CommonTextStyle.size18Bold.copyWith(color:Color(0xFF575E67)),
                  ),
                  Text(
                    nv.tenNhanVien,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: CommonTextStyle.size18Bold.copyWith(color:Color(0xFFCC8053)),
                  ),
                ],
              ),
              SizedBox(height: 3,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      'Số hoá đơn: ',
                      style: CommonTextStyle.size20Bold.copyWith(fontSize: 14, color:Color(0xFF575E67))
                  ),
                  Text(
                    nv.soHoaDon.toString(),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.end,
                    style: CommonTextStyle.size20Bold.copyWith(fontSize: 12, color:Color(0xFF575E67))
                  ),
                ],
              ),
              SizedBox(height: 3,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      'Doanh thu: ',
                      style: CommonTextStyle.size20Bold.copyWith(fontSize: 14, color:Color(0xFF575E67))
                  ),
                  Text(
                      '${numberToMoneyString1(nv.doanhThu)}đ',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.end,
                      style: CommonTextStyle.size20Bold.copyWith(fontSize: 12, color:Color(0xFF575E67))
                  ),
                ],
              ),
            ],
          )
      ),
    );
  }

}

