
import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/plugin/dio.dart';
import 'package:dio/dio.dart';

class AuthService {

  static Future<Response> login(Map data) async {
    try {
      var response = await http.post(Constant.apiUrl + 'login', data: data);
      return response;
    } on DioError catch (e) {
      print(e.toString());
      return null;
    }
  }
}
