
import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/common/language-config.dart';
import 'package:cafe_management/models/common/system-config.dart';
import 'package:cafe_management/plugin/dio.dart';
import 'package:cafe_management/response/response-data.dart';

class ConfigService {

  static Future<List<LanguageConfig>> getConfig() async {
    try {
      final response = await http.get('https://s3-ap-southeast-1.amazonaws.com/language.io/exchange/config.json');
      var data = response.data;
      List<dynamic> list = List.from(data);
      List<LanguageConfig> configs = list.map((config) => LanguageConfig.fromJson(config)).toList();
      return configs;
    } catch (e) {
      return [];
    }
  }

  // static Future<SystemConfig> getSystemConfig() async {
  //   try {
  //     var response = await http.get(Constant.apiUrl + 'system/system-config');
  //     ResponseData responseData = response.data;
  //     SystemConfig config = SystemConfig.fromJson(responseData.data);
  //     return config;
  //   } catch (e) {
  //     return new SystemConfig();
  //   }
  // }

}
