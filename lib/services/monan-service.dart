import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/hoadon/hoadon-when-get.dart';
import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/plugin/dio.dart';
import 'package:dio/dio.dart';

abstract class MonAnService {
  static Future<List<MonAn>> getAllMonAn() async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/monan');
      List<dynamic> data = List.from(response.data);
      List<MonAn> liMonAn = data.map((e) => MonAn.fromJson(e)).toList();
      return liMonAn;
    } catch (e) {
      return [];
    }
  }

  static Future<List<MonAn>> getAllMonAn1() async {
    try {
      final response = await http.get(Constant.apiUrl +'staff/monan?loaimon=4');
      List<dynamic> data = List.from(response.data);
      List<MonAn> liMonAn = data.map((e) => MonAn.fromJson(e)).toList();
      return liMonAn;
    } catch (e) {
      return [];
    }
  }

  static Future<List<MonAn>> getAllMonAn2() async {
    try {
      final response = await http.get(Constant.apiUrl +'staff/monan?loaimon=5');
      List<dynamic> data = List.from(response.data);
      List<MonAn> liMonAn = data.map((e) => MonAn.fromJson(e)).toList();
      return liMonAn;
    } catch (e) {
      return [];
    }
  }

  static Future<List<MonAn>> getAllMonAn3() async {
    try {
      final response = await http.get(Constant.apiUrl +'staff/monan?loaimon=6');
      List<dynamic> data = List.from(response.data);
      List<MonAn> liMonAn = data.map((e) => MonAn.fromJson(e)).toList();
      return liMonAn;
    } catch (e) {
      return [];
    }
  }

  static Future<dynamic> addMonAn(Map map) async {
    try {
      final response = await http.post(Constant.apiUrl + 'admin/monan', data: map);
      return response;
    } catch (e) {
      return null;
    }
  }

  static Future<dynamic> editMonAn(Map map) async {
    try {
      final response = await http.put(Constant.apiUrl + 'admin/monan', data: map);
      return response;
    } catch (e) {
      return null;
    }
  }

  static Future<dynamic> addHoaDon(Map map) async {
    try {
      final response = await http.post(Constant.apiUrl + 'admin/hoadon', data: map);
      return response;
    } catch (e) {
      return null;
    }
  }

  static Future<dynamic> editHoaDon(Map map) async {
    try {
      final response = await http.put(Constant.apiUrl + 'admin/hoadon', data: map);
      return response;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  static Future<List<dynamic>> getHoaDon() async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/hoadon');

      return response.data;
    } catch (e) {
      return null;
    }
  }
}