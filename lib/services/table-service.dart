import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/table/table.dart';
import 'package:cafe_management/plugin/dio.dart';
import 'package:dio/dio.dart';


abstract class TableService {
  static Future<List<TableCoffee>> getAllTable() async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/ban');
      List<dynamic> data = List.from(response.data);
      List<TableCoffee> liTable = data.map((e) => TableCoffee.fromJson(e)).toList();
      return liTable;
    } catch (e) {
      return [];
    }
  }

  static Future<Response> editBan(Map map) async {
    try {
      final response = await http.put(Constant.apiUrl + 'admin/ban', data: map);
      return response;
    } catch (e) {
      return null;
    }
  }
}
