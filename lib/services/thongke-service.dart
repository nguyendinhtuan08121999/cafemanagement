import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/thongke/thongke-all.dart';
import 'package:cafe_management/models/thongke/thongke.dart';
import 'package:cafe_management/plugin/dio.dart';

abstract class ThongKeService {
  static Future<ThongKeAll> getAllThongKe(DateTime start, DateTime end) async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/thongke/all?from=$start&to=$end');
      ThongKeAll thongKe = ThongKeAll.fromJson(response.data);
      return thongKe;
    } catch (e) {
      return new ThongKeAll();
    }
  }
}