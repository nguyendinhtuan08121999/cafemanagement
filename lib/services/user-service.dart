import 'package:cafe_management/config/constant.dart';
import 'package:cafe_management/models/user/user.dart';
import 'package:cafe_management/plugin/dio.dart';
import 'package:cafe_management/utils/shared-preference.dart';


abstract class UserService {
  static Future<User> getUser(String str) async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/taikhoan/$str');
      Map<String, dynamic> map = response.data;
      //print('map' + map);
      print('reponse: $response');
      User user = User.fromJson(map);
      await SharedPrefsService.setUser(map);
      return user;
    } catch (e) {
      return null;
    }
  }

  // static Future<Response> sendVerificationCode(dynamic payload) async {
  //   try {
  //     final response = await http.post(Constant.apiUrl + 'user/send-verification-code', data: payload);
  //     return response;
  //   } catch (e) {
  //     return null;
  //   }
  // }
}
