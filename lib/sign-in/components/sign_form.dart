import 'package:cafe_management/providers/common/token-provider.dart';
import 'package:cafe_management/providers/hoadon/hoadon-provider.dart';
import 'package:cafe_management/providers/table/table-provider.dart';
import 'package:cafe_management/providers/user/user-provider.dart';
import 'package:cafe_management/router/routing-name.dart';
import 'package:cafe_management/screens/layout/main.dart';
import 'package:cafe_management/services/auth-service.dart';
import 'package:cafe_management/utils/dialog-builder.dart';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:cafe_management/widgets/defauld-button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final formLoginKey = GlobalKey<FormState>();
  TextEditingController accountController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  String email;
  String password;
  bool remember = false;
  bool autoValidate = false;
  final List<String> errors = [];

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formLoginKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(height: 30),
          buildPasswordFormField(),
          SizedBox(height: 50),
          DefaultButton(
            text: "Login",
            press: () {
              _handleLogin().then((value) {
                if (value){
                  if (Provider.of<UserProvider>(context, listen: false).user.role == "CUSTOMER"){
                    Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.homeCustomer, (route) => false);
                  } else
                  Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.homeRoute, (route) => false);
                }
              });
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      autovalidate: autoValidate,
      controller: passController,
      obscureText: true,
      validator: (value) {
        if (value == null || value == '') return 'Field is not empty';
        return null;
      },
      decoration: InputDecoration(
        labelStyle: TextStyle(color: Colors.brown),
        labelText: "Password",
        hintText: "Enter your password",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: EdgeInsets.symmetric(horizontal: 42, vertical: 20),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.red),
          gapPadding: 10,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        //suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      validator: (value) {
        if (value == null || value == '') return 'Field is not empty';
        return null;
      },
      controller: accountController,
      autovalidate: autoValidate,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: EdgeInsets.symmetric(horizontal: 42, vertical: 20),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.red),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(28),
          borderSide: BorderSide(color: Colors.brown),
          gapPadding: 10,
        ),
        labelStyle: TextStyle(color: Colors.brown),
        labelText: "Account",
        hintText: "Enter your account",
        //suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  Future<bool> _handleLogin() async {
    if (!formLoginKey.currentState.validate()) {
      setState(() {
        autoValidate = true;
      });
      return false;
    }
    //DialogBuilder.buildLoadingDialog();
    Map map = new Map();
    map['userName'] = accountController.text;
    map['password'] = passController.text;
    print(map.toString());

    final response = await AuthService.login(map);

    if (response.statusCode == 200) {
      await Provider.of<TokenProvider>(context, listen: false).setTokenObj(response.data);
      await Provider.of<UserProvider>(context, listen: false).getUser(map['userName']);
      await Provider.of<HoaDonProvider>(context, listen: false).getHoaDon();
      SnackbarBuilder.showSnackbar(content:'Success', color: Colors.blue);
      return true;
    } else {
      Navigator.of(context).pop();
      SnackbarBuilder.showSnackbar(content:'Wrong password or username', color: Colors.red);
      return false;
    }

  }
}
