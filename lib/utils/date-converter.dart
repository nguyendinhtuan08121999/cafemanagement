import 'package:cafe_management/config/constant.dart';
import 'package:intl/intl.dart';

class DateConverter {
  static String convertDate(num timestamp) {
    try {
      if (timestamp == null || timestamp == 0)
        return 'N/A';
      var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
      return DateFormat(Constant.formatDateHour).format(date);
    } catch (e) {
      return '';
    }
  }

  static String convertDecimalHoursToHHmm(double hours) {
    try {
      int hour = hours.floor();
      int minute = ((hours - hour) * 60).round();
      if(hour != 0 || minute != 0) {
        var date = DateTime(0,0,0,hour,minute);
        var dateString = DateFormat('Hm').format(date);
        return dateString;
      }
      return '00:00';
    } catch(e) {
      return '00:00';
    }
  }
}
