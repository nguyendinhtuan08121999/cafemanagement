import 'dart:convert';
import 'package:cafe_management/config/theme.dart';
import 'package:cafe_management/models/common/token.dart';
import 'package:cafe_management/models/user/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsService {
  static TokenObj tokenObj;

  static Future<TokenObj> setTokenObj(String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', str);
    //Map map = jsonDecode(str);
    tokenObj = TokenObj.fromJson(str);
    return tokenObj;
  }

  static Future<TokenObj> getTokenObj() async {
    try {
      if (tokenObj?.accessToken == null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.getString('token');
        if (token == null || token == '') {
          return null;
        }
        //Map map = jsonDecode(token);
        tokenObj = TokenObj.fromJson(token);
        return tokenObj;
      } else {
        return tokenObj;
      }
    } catch (e) {
      return null;
    }
  }

  static removeToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
  }

  static void setThemeMode(AppTheme theme) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt('themeMode', AppTheme.values.indexOf(theme));
  }

  static Future<ThemeData> getThemeMode() async {
    var prefs = await SharedPreferences.getInstance();
    int preferredTheme = prefs.getInt('themeMode') ?? 1;
    ThemeData _themeData = appThemeData[AppTheme.values[preferredTheme]];
    return _themeData;
  }

  static Future<User> getUser() async {
    var prefs = await SharedPreferences.getInstance();
    String jsonUser = prefs.getString('user');
    if (jsonUser == null) return null;
    return User.fromJson(jsonDecode(jsonUser));
  }

  static Future setUser(Map user) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('user', jsonEncode(user));
  }

  static removeUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
  }
}
