abstract class StringUtils {
  // String utils
  static String generateChars (numberDecimal, { String char }) {
    String temp = '00000000000000000000000000000000000000000000';
    if (char == null) {
      return temp.substring(0, numberDecimal);
    } else {
      String str = temp.replaceAll('0', char);
      return str.substring(0, numberDecimal);
    }
  }
  
  static String toStringAsFixed(dynamic value, num precision) {
    try {
      String str = value.toString();
      bool isNumberDecimal = str.indexOf('.') != -1;
      if (!isNumberDecimal) {
        return str + '.' + generateChars(precision);
      }
      List<String> arr = str.split('.');
      String secondStr = arr[1];
      if (secondStr.length >= precision) {
        return arr[0] + '.' + arr[1].substring(0, precision);
      }
      return arr[0] + '.' + arr[1] + generateChars(precision - arr[1].length);
    } catch (e) {
      return '0' + '.' + generateChars(precision ?? 2);
    }
  }
}
