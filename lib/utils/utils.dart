import 'dart:math';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'snackbar-builder.dart';

abstract class UtilService {

  static bool checkValidDecimals(value, precision) {
    value = num.parse(value);
    if (value.floor() == value) {
      return true;
    }
    try {
      var arr = value.toString().split('.');
      return arr[1].length <= precision || false;
    } catch (error) {
      return false;
    }
  }

  static bool isNumeric(dynamic s) {
    s = s.toString();
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  static bool isLargerZero(dynamic s) {
    s = s.toString();
    return num.parse(s) > 0;
  }

  static bool checkEvenNumber(index) {
    return index % 2 == 0;
  }

  static int compareTo(dynamic origin, dynamic other) {
    origin = num.parse(origin.toString());
    other = num.parse(other.toString());
    return origin.compareTo(other);
  }

  static bool isLarger(dynamic origin, dynamic other) {
    int equal = compareTo(origin, other);
    return equal == 1;
  }

  static bool isSmaller(dynamic origin, dynamic other) {
    int equal = compareTo(origin, other);
    return equal == -1;
  }

  static bool isEqual(dynamic origin, dynamic other) {
    int equal = compareTo(origin, other);
    return equal == 0;
  }

  static bool checkValidAmountDecimals(dynamic value, int precision) {
    try {
      List arr = value.toString().split('.');
      if (arr.length <= 1) return true;
      return arr[1].toString().length <= precision || false;
    } catch (error) {
      return false;
    }
  }

  static Map getTimeWithPrevHours(timeMinus) {
    dynamic now = new DateTime.now().millisecondsSinceEpoch / 1000 + 60;
    dynamic from = now - timeMinus;
    return {'from': int.parse(from.toStringAsFixed(0)), 'to': int.parse(now.toStringAsFixed(0))};
  }

  static String getCurrentMonth() {
    try {
      var now = new DateTime.now();
      String month = DateFormat('MM/yyyy').format(now);
      return month;
    } catch (e) {
      return '';
    }
  }

  static String convertMillisecondsToFullTime(int milliseconds) {
    try {
      var date = DateTime.fromMillisecondsSinceEpoch(milliseconds);
      var dateString = DateFormat.yMMMd().format(date);
      var timeString = DateFormat.Hms().format(date);
      return dateString + ' ' + timeString;
    } catch (error) {
      print(error);
      return '';
    }
  }

  static String convertMillisecondsToTimeWithType(int milliseconds, { String type = 'dd/MM/yyyy' }) {
    try {
      var format = new DateFormat(type);
      var date = DateTime.fromMillisecondsSinceEpoch(milliseconds * 1000);
      var time = '';
      time = format.format(date);
      return time;
    } catch (e) {
      return '';
    }
  }

  static String convertMillisecondsToTime(int milliseconds) {
    try {
      var date = DateTime.fromMillisecondsSinceEpoch(milliseconds);
      DateTime now = DateTime.now();
      var dateString = '';
      var timeString = DateFormat.ms().format(date);
      if (date.year == now.year) {
        dateString = DateFormat.Md().format(date);
      } else {
        dateString = DateFormat.yMMMd().format(date);
      }
      return timeString + ' ' + dateString;
    } catch (error) {
      print(error);
      return '';
    }
  }

  static double roundDown(num value, int precision) {
    value = value.toDouble();
    final isNegative = value.isNegative;
    final mod = pow(10.0, precision);
    final roundDown = (((value.abs() * mod).floor()) / mod);
    return isNegative ? -roundDown : roundDown;
  }

  static String formatNumber(String str) {
    if(str == null || str == '')
      return '0';
    double number = double.parse(str);
    var f = new NumberFormat('###,##0');
    return f.format(number);
  }
}
