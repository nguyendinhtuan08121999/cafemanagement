import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnimatedLogo extends AnimatedWidget {
  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        child: SvgPicture.asset('images/icon/ic_loo.svg'),
      ),
    );
  }
}