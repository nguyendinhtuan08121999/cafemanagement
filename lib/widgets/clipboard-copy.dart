import 'dart:async';
import 'package:cafe_management/utils/snackbar-builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class ClipboardCopier extends StatefulWidget {
  final String content;

  ClipboardCopier({this.content});

  @override
  _ClipboardCopierState createState() => _ClipboardCopierState();
}

class _ClipboardCopierState extends State<ClipboardCopier> {
  bool _condition = true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Icon(Icons.content_copy),
      onTap: _condition
        ? () {
            setState(() => _condition = false);
            Timer(Duration(seconds: 3), () => setState(() => _condition = true));
            print(_condition);
            Clipboard.setData(ClipboardData(text: widget.content)).then((result) {
              SnackbarBuilder.showSnackbar(
                content: 'Copied to clipboard',
                color: Theme.of(context).accentColor
              );
            });
          }
        : null,
    );
  }
}
