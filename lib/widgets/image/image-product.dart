import 'package:flutter/material.dart';

class ImageProduct extends StatelessWidget {
  final String url;
  final double width;
  final double height;

  ImageProduct({ @required this.url, this.width, this.height });
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Container(
        width: width ?? double.infinity,
        height: height ?? double.infinity,
        decoration: BoxDecoration(
          color: Color(0xFF3967D5).withOpacity(0.15),
        ),
        child: Image.asset(url, fit: BoxFit.cover,)
      ),
    );
  }
}
