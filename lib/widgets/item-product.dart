import 'package:cafe_management/models/monan/monan.dart';
import 'package:cafe_management/screens/ordertable/widget/shopping_cart_tab.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'image/image-product.dart';

class ItemProduct extends StatefulWidget {
  final MonAn monAn;
  final Function press;

  ItemProduct({ this.monAn, this.press});
  @override
  _ItemProductState createState() => _ItemProductState();
}

class _ItemProductState extends State<ItemProduct> {

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.press ?? (){},
      child: Container(
        margin: const EdgeInsets.only(right: 10),
        width: 135,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).canvasColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildContainImageProduct(),
            Padding(
              padding: const EdgeInsets.only(left:6.0),
              child: Text(
                widget.monAn.tenMonAn,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            const SizedBox(height: 5,),
            _buildAmount(),
            const SizedBox(height: 5,),
            _buildPriceAndButton(),
          ],
        ),
      )
    );
  }

  Widget _buildContainImageProduct() {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: ImageProduct(
        url: "images/anh${widget.monAn.maMonAn}.jpg",
        height: 90,
      )
    );
  }

  Widget _buildAmount() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 5),
      color: Color(0xffF8F9FC),
      child: Text(
        'Let\'s enjoy',
        style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 10,fontFamily: 'Lato', color: Theme.of(context).accentColor),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildPriceAndButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, top: 10, right: 4, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              Text(
                '${numberToMoneyString1(widget.monAn.donGia)}',
                style: Theme.of(context).textTheme.button.copyWith(
                  fontSize: 18,
                  color: Colors.brown,
                  fontWeight: FontWeight.w700
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  'đ',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                    fontWeight: FontWeight.w300,
                    fontSize: 10,
                    color: Colors.brown,
                  ),
                ),
              ),
            ],
          ),
          SvgPicture.asset('images/ic_addition.svg')
        ],
      ),
    );
  }

}
