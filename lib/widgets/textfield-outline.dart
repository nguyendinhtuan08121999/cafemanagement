import 'package:cafe_management/styles/component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class TextFieldOutLine extends StatelessWidget {
  final String hintText;
  final int maxLine;
  final int maxLength;
  final Function onChanged;
  final bool isEnable;
  final TextEditingController controller;
  final String filterPattern;
  final bool autoValidate;
  final Widget suffixIcon;

  TextFieldOutLine({
    this.isEnable,
    this.controller,
    this.onChanged,
    this.hintText,
    this.filterPattern = r'.',
    this.maxLength,
    this.maxLine,
    this.autoValidate = false,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.symmetric(vertical: 15),
      margin: EdgeInsets.only(bottom: 5),
      child: TextFormField(
        controller: controller,
        maxLines: maxLine,
        enabled: isEnable,
        autovalidate: autoValidate,
        maxLength: maxLength,
        inputFormatters: [FilteringTextInputFormatter.allow(RegExp(filterPattern))],
        textAlignVertical: TextAlignVertical.center,
        validator: (value) {
          if (value == null || value == '') return 'Field is not empty';
          return null;
        },
        style: CommonTextStyle.size14Light,
        decoration: InputDecoration(
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.grey, width: 1,),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.brown.withOpacity(0.7), width: 1,),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.brown.withOpacity(0.7), width: 1),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Theme.of(context).errorColor, width: 1),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Theme.of(context).errorColor, width: 1),
          ),
          contentPadding: const EdgeInsets.only(top: 20, left: 10, right: 10),
          hintText: hintText,
          hintStyle: CommonTextStyle.size14Light.copyWith(color: Theme.of(context).hintColor),
          suffixIcon: suffixIcon,
          errorMaxLines: 3,
          errorStyle: TextStyle(
            color: Theme.of(context).errorColor,
            fontStyle: FontStyle.italic,
          ),
          suffixIconConstraints: BoxConstraints(
            minHeight: 16,
          ),

        ),
      ),
    );
  }

}
