import 'package:cafe_management/styles/component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldUnderline extends StatelessWidget {
  final String hintText;
  final bool isPass;
  final Function onChanged;
  final String filterPattern;
  final TextEditingController controller;
  final bool autoValidate;

  TextFieldUnderline({
    Key key,
    this.hintText,
    this.isPass,
    this.onChanged,
    this.filterPattern = r'.',
    this.controller,
    this.autoValidate = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      autovalidate: autoValidate,
      obscureText: isPass == null ? false : true,
      validator: (value) {
        if (value == null || value == '') return 'Field is not empty';
          return null;
      },
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp(filterPattern))],
      decoration: InputDecoration(
        hintText: hintText,
        enabledBorder: _buildUnderlineInputBorder(color: CommonColor.colorBluePastel),
        focusedBorder: _buildUnderlineInputBorder(color: CommonColor.colorBlue),
        errorBorder: _buildUnderlineInputBorder(color: Theme.of(context).errorColor.withOpacity(0.6)),
        focusedErrorBorder: _buildUnderlineInputBorder(color: Theme.of(context).errorColor)
      ),
      onChanged: (val) {
        this.onChanged(val);
      },
    );
  }

  _buildUnderlineInputBorder({ Color color }) 
    => UnderlineInputBorder(
      borderSide: new BorderSide(
        color: color ?? CommonColor.colorBluePastel,
      ),
    );
}
